# matrix prettyprint
def matrix_pretty_print(m)
    m.each() do |r| 
        puts r.map{|e| e.to_s(16)}.join("\t")
    end
end