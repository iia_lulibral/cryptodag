#!/usr/bin/ruby

require_relative '../cryptosystems/midori128/midori128.rb'
require_relative "../writers/step1diff"
require_relative "../backends/minizinc"

## generates minizinc models for midori
#
def main
    for i in 3..20
        dag = Midori128_Dag.new(nb_rounds=i)
        file = File.open("tmp/diff_midori_#{i}.mzn", "w")
        model = Step1Diff.new.generate_extended_model(dag, nb_rounds)
        file.puts Minizinc.new.generate_code(model)
        file.close
    end
end

main