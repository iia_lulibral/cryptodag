#!/usr/bin/ruby

require_relative '../cryptosystems/midori128/midori128.rb'
require_relative '../writers/minizinc.rb'

## generates minizinc models for midori
#
def main
    for i in 3..20
        dag = Midori128_Dag.new(nb_rounds=i)
        file = File.open("tmp/midori_#{i}.mzn", "w")
        file.puts get_minizinc_dk_step1opt(Midori128_Dag.new(nb_rounds=i))
        file.close
    end
end

main