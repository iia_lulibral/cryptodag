require "slop"

def process_options(arg)
  begin
    opts = Slop.parse(arg) do |o|
      o.string "-l", "--language", "Modeling Language (Supported: minizinc (default) or picat)", default: "minizinc"
      o.bool "-c", "--countdown", "Use Countdown Backup", default: false
      o.bool "-o", "--optimize", "Suppress duplicate variables"

      o.on "--help", "print the help" do
        puts o
        exit
      end
    end
  rescue Slop::Error => error
    $stderr.puts error
    exit(2)
  end
  opts
end

$options = process_options(ARGV)
