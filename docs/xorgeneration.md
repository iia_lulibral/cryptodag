# XOR generation

## conjecture 1:

Given a set of xor clauses, all generated clauses of size <= 4 can be found by a "path" that does not contain any clause of size > 4. If this conjecture is true, we can benefit from a strong abstraction with a ``little'' generation cost.

## counter-example for conjecture 1:

input clauses: 
```
{{21, 29, 31}, {13, 15, 35}, {21, 22, 23}, {15, 17, 32}, {27, 29, 36}, {3, 5, 31}, {5, 7, 32}, {1, 2, 3}, {11, 12, 13}, {7, 9, 33}, {1, 9, 34}, {11, 19, 34}, {17, 19, 36}, {23, 25, 35}, {25, 27, 33}}```
```

set of clauses of size <= 4 we obtain if we do not consider clauses of size >= 5 (37 clauses):
```
{{2, 3, 9, 34}, {1, 7, 33, 34}, {15, 19, 32, 36}, {7, 9, 33}, {1, 2, 5, 31}, {1, 9, 11, 19}, {3, 5, 21, 29}, {1, 2, 3}, {11, 12, 13}, {21, 22, 23}, {15, 17, 32}, {12, 13, 19, 34}, {25, 29, 33, 36}, {5, 9, 32, 33}, {22, 23, 29, 31}, {}, {23, 27, 33, 35}, {21, 29, 31}, {23, 25, 35}, {7, 9, 25, 27}, {3, 5, 31}, {1, 9, 34}, {5, 7, 32}, {27, 29, 36}, {17, 19, 36}, {13, 17, 32, 35}, {17, 19, 27, 29}, {11, 17, 34, 36}, {3, 7, 31, 32}, {11, 12, 15, 35}, {25, 27, 33}, {13, 15, 35}, {13, 15, 23, 25}, {21, 22, 25, 35}, {21, 27, 31, 36}, {11, 19, 34}, {5, 7, 15, 17}}
```

set of clauses <= 4 we obtain by also considering clauses of size >= 5 (41 clauses):
```
{{12, 13, 19, 34}, {2, 3, 9, 34}, {13, 15, 23, 25}, {21, 22, 23}, {22, 23, 29, 31}, {11, 17, 34, 36}, {}, {3, 7, 31, 32}, {13, 15, 35}, {15, 17, 32}, {25, 27, 33}, {1, 9, 34}, {1, 2, 5, 31}, {1, 3, 12, 22}, {11, 19, 34}, {5, 9, 32, 33}, {21, 29, 31}, {1, 9, 11, 19}, {21, 22, 25, 35}, {3, 5, 21, 29}, {11, 12, 15, 35}, {2, 12, 21, 23}, {1, 7, 33, 34}, {17, 19, 36}, {7, 9, 25, 27}, {15, 19, 32, 36}, {23, 25, 35}, {13, 17, 32, 35}, {17, 19, 27, 29}, {25, 29, 33, 36}, {7, 9, 33}, {3, 5, 31}, {21, 27, 31, 36}, {2, 12, 22}, {11, 12, 13}, {5, 7, 15, 17}, {27, 29, 36}, {1, 2, 3}, {23, 27, 33, 35}, {5, 7, 32}, {2, 11, 13, 22}}
```

For instance, in this case, the limitation does not allow to infer the clause `{2, 12, 22}`, but this clause can be obtained by xor-ing all the input clauses.