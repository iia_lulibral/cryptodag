require "minitest/autorun"
require "pry"

require_relative "../minizinc.rb"
require_relative "../../constraints/all"

class TestBackend < MiniTest::Unit::TestCase
  def setup
    @model = Model.new
    @minizinc = Minizinc.new
    @output = %(\noutput[""];\n)
  end

  def test_empty_model
    assert_equal(@output, @minizinc.generate_code(@model))
  end

  def build_DTable(values = nil, constant: false)
    v = DTable.new("hector", 2..3000, [2, 2], values, constant: constant)

    @model.add_variables(v)
    @model.add_variables(v[0, 1])

    @model.add_constraints(
      Equality.new(
        v[0, 1],
        v[0, 0]
      )
    )
    v
  end

  def test_dynamic_table_if_unfolded
    v = build_DTable

    v.declarationMode = :unfolded
    code = @minizinc.generate_code(@model)
    refute_match("array[1..2, 1..2] of var 2..3000: hector ;", code)
    assert_match("var 2..3000: hector_0_1;", code)
    assert_match("constraint hector_0_1  = hector_0_0;", code)
  end

  def test_dynamic_table_if_folded
    v = build_DTable

    v.declarationMode = :folded
    code = @minizinc.generate_code(@model)
    refute_match("var 2..3000: hector_0_1;", code)
    assert_match("array[1..2, 1..2] of var 2..3000: hector ;", code)
    assert_match("constraint hector[1,2]  = hector[1,1];", code)
  end

  def test_dynamic_table_if_value
    v = build_DTable([1917, 1789, 1848, 2042])

    v.declarationMode = :folded
    code = @minizinc.generate_code(@model)
    refute_match("var 2..3000: hector_0_1;", code)
    assert_match("array[1..2, 1..2] of var 2..3000: hector = array2d(1..2, 1..2, [1917, 1789, 1848, 2042]);", code)
    assert_match("constraint hector[1,2]  = hector[1,1];", code)
  end

  def test_dynamic_table_if_folded_constant
    v = build_DTable([1917, 1789, 1848, 2042], constant: true)

    v.declarationMode = :folded
    code = @minizinc.generate_code(@model)
    refute_match("2..3000: hector_0_1;", code)
    assert_match("array\[1..2, 1..2] of 2..3000: hector = array2d(1..2, 1..2, [1917, 1789, 1848, 2042]);", code)
    assert_match("constraint hector[1,2]  = hector[1,1];", code)
  end

  def test_dynamic_table_if_value_constant
    v = build_DTable([1917, 1789, 1848, 2042], constant: true)

    v.declarationMode = :value
    code = @minizinc.generate_code(@model)
    refute_match("2..3000: hector_0_1;", code)
    refute_match("array[1..2, 1..2] of 2..3000: hector = array2d(1..2, 1..2, [1917, 1789, 1848, 2042]);", code)
    assert_match("constraint 1789  = 1917;", code)
  end
end
