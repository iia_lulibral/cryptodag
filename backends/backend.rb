require "singleton"

require_relative "../constraints/model"
require_relative "../constraints/constraints"
require_relative "../constraints/variables"

## Backends take models to generate a CSP model (like minizinc)
class Backend
  def initialize
    @types = { "int" => "int", "bool" => "bool" }
  end

  ## Generate specialized code for a solver
  # @param model The model to parse
  def generate_code(model)
    model.build_declarations

    str = before_model()
    str += (model.declarations + model.constraints + model.instructions).map do |v|
      visit(v)
    end.join("") + "\n"
    str += generate_display(model.to_display) + "\n"
    str += after_model()
  end

  ## Called before the model is parsed
  protected def before_model()
    ""
  end

  ## Called after the model is parsed
  protected def after_model
    ""
  end

  ## Parse the model
  # @param e [ element ] Element to visit (constraint or declaration)
  protected def visit(e)
    raise "Must be implemented"
  end

  # Generates code to display variables
  #
  # @param displays [ An array of Display ] : Array with all the Display
  # @return The solver code
  protected def generate_display(displays)
    ""
  end
end
