require "singleton"

require_relative "backend"
require_relative "../tools/multimatrix.rb"

class Minizinc < Backend
  # visit a constraint element by building its text representation

  private def one_dim2two_dim(array, one_dim_size)
    throw "Dimension Error" unless array.size % one_dim_size == 0
    nb_slice = array.size / one_dim_size
    slices = []
    nb_slice.times do |sl|
      slices << array.slice(one_dim_size * sl, one_dim_size)
    end
    slices
  end

  ## generate a string representing a declaration or a constraint
  # @return [ string ]
  def visit(e)
    # $stderr.puts e.inspect
    case e
    when NoDeclaration
    when CSetDeclaration
      v = e.variable
      "set of int : #{visit(v.name)} = #{visit(v.type)};\n"
    when ConstantDeclaration
      v = e.variable
      "#{visit(v.type)}: #{visit(v.name)} = #{visit(v.value)} ;\n"
    when ConstantTableDeclaration
      v = e.variable
      dim = v.dimensions.map { |d| "1..#{d}" }.join(", ")
      if (v.value)
        value = "= array#{v.dimensions.size}d(#{dim}, [#{v.value.map(&:to_s).join(", ")}])"
      else
        value = ""
      end
      "array[#{dim}] of #{visit(v.type)}: #{visit(v.name)} #{value};\n"
    when TableDeclaration
      v = e.variable
      dim = v.dimensions.map { |d| "1..#{d}" }.join(", ")
      if (v.value)
        value = "= array#{v.dimensions.size}d(#{dim}, [#{v.value.map(&:to_s).join(", ")}])"
      else
        value = ""
      end
      "array[#{dim}] of var #{visit(v.type)}: #{visit(v.name)} #{value};\n"
    when SimpleVariableDeclaration
      v = e.variable
      if v.value
        "var #{visit(v.type)}: #{visit(v.name)} = #{visit(v.value)};\n"
      else
        "var #{visit(v.type)}: #{visit(v.name)};\n"
      end
    when Comment
      "% #{e.value}\n"
    when VariableInTable
      variable_in_table(e)
    when DynamicVariable
      case e.declarationMode
      when :folded
        variable_in_table(e)
      when :unfolded
        e.name
      when :value
        e.value
      else
        raise "Unknown mode #{e.declarationMode}"
      end
    when Variable
      e.name
    when SetValue
      "{ #{e.values.map { |v| visit(v) }.join(", ")} }"
    when Constant
      e.name
    when Different
      "constraint #{visit(e.v1)} != #{visit(e.v2)};\n"
    when Equality
      "constraint #{visit(e.v1)}  = #{visit(e.v2)};\n"
    when IncludedIn
      "constraint #{visit(e.expression)} in #{visit(e.set)};\n"
    when Equivalence
      "constraint #{visit(e.v1)} <-> #{visit(e.v2)};\n"
    when Instruction
      "#{e.str};\n"
    when Interval
      "#{e.min}..#{e.max}"
    when Operator
      e.operator.to_s
    when Type
      raise "Unknown type for minizinc: #{e.name} . Known types are #{@types}" unless @types.include?(e.name)
      (@types[e.name]).to_s
    when TableSum
      raise "TableSum: indices does not have the same dimensions as the input table." unless e.indices.length == e.table.dimensions.length
      indice_iteration = e.indices.length.times.map { |i| "#{e.indices[i]} in #{1..e.table.dimensions[i]}" }.compact
      res = "sum(#{indice_iteration.join(", ")})"
      res += "(#{e.table.name}[#{e.indices.join(",")}])"
      return res
    when Name
      e.name.to_s
    when Operation
      e.parameters.map { |p| visit(p) }.join(" #{visit(e.name)} ").to_s
    when Priority
      "(#{visit(e.parameter)})"
    when Function
      "#{visit(e.name)}(#{e.parameters.map { |p| visit(p) }.join(", ")})"
    when String
      e
    when LitteralValue
      visit(e.value)
    when Integer
      e.to_s
    when TrueClass
      "true"
    when FalseClass
      "false"
    else
      binding.pry
      raise "Unknown type (#{e.class}) for #{e}"
    end
  end

  def variable_in_table(e)
    index = e.indexes.map { |index| index + 1 }
      .map { |index| visit(index) }
      .join(",")
    "#{visit(e.table.name)}[#{index}]"
  end

  def simple_variable
  end

  def display_array2D(m)
    a = []
    m.size.times do |l|
      m.size.times do |c|
        a << "\\( #{m.name}[#{l} + 1,#{c} + 1] )"
      end
    end
    a.join(", ")
  end

  # Generates code to display variables
  #
  # @param displays [ An array of Display ] : Array with all the Display
  # @return The solver code
  def generate_display(displays)
    str = 'output["'

    str += displays.map do |display|
      case display
      when Display_Array2D
        vs = display.matrix.each_index do |line, column|
          "\\( #{display.name}[#{line} + 1,#{column} + 1] )"
        end.to_a.join(", ")
        "#{display.text} = #{vs}\\n"
      when Display_Cipher
        vs = display.variables.map do |v|
          case v
          when MultiMatrix
            display_matrix(v)
          when Array2D
            display_array2D(v)
          else
            "\\( #{visit(v)})"
          end
        end.join(", ")
        "#{display.text} = #{vs}\\n"
      end
    end.join("")

    str += '"];'

    str
  end
end

## Unused
def display_matrix(m)
  case m.dimensions.size
  when 1
    m.map { |v| "#{visit(v)} " }.join(", ")
  when 2
    m.each_index do |l, c|
      s = (c == 0 ? "\n" : "")
      s += "\\( #{m.name}[#{l} + 1,#{c} + 1] )"
    end.join(", ")
  else
    raise "Can't display #{m.dimensions.size}D matrices!"
  end
end
