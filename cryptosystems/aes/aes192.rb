#!/usr/bin/ruby

require_relative './constants.rb'

require_relative '../../cryptodag.rb'
require_relative '../../simulate_cryptodag.rb'

require_relative '../../nodes/input.rb'
require_relative '../../nodes/xor.rb'
require_relative '../../nodes/subbytes.rb'
require_relative '../../nodes/shiftrows.rb'
require_relative '../../nodes/mixcolumn.rb'
require_relative '../../nodes/aeskeyschedule.rb'


##
# combine the 2 first columns of m1 starting at x coordinate i1 and the 2 last columns of m2 starting at x coordinate i2
def combine_columns_2h(m1, i1, m2, i2)
    matrix = MultiMatrix.build(m1.type, [4,4])
    for i in 0..1
        4.times do |j|
            matrix[j][i] = m1[j][i1+i]
        end
    end
    for i in 0..1
        4.times do |j|
            matrix[j][i+2] = m2[j][i2+i]
        end
    end
    return matrix
end

def extract_four_first(m1)
    matrix = MultiMatrix.build(m1.type, [4,4])
    for i in 0..3
        4.times do |j|
            # matrix[j][i] = m1[j][i]
            matrix[i][j] = m1[j][i]
        end
    end
    return matrix
end


# implements the AES 192 DAG
class AES192_Dag < CryptoDag

    def initialize(nb_rounds=12)
        # prepare constants
        @rcon = rcon
        @mc = mixcolumns_matrix()
        @sbox = s_box()
        # define input nodes
        nodes = []
        @nb_rounds = nb_rounds
        @x = InputNode.new(name:"X", dimensions:[4,4])
        nodes.push(@x)
        @k = InputNode.new(name:"K", dimensions:[4,6])
        nodes.push(@k)
        key_nodes = []
        key_nodes.push(@k)
        for i in  0..(nb_rounds/1.5).ceil
            if i > 0
                k_current = AESKeyScheduleNode.new(
                    name:"K_#{i}",
                    input:key_nodes[key_nodes.length-1].outputs[0],
                    subtable:@sbox,rcon:@rcon[i]
                )
                key_nodes.push(k_current)
                nodes.push(k_current)
            end
        end
        # puts "keynodes size: #{key_nodes.length}"
        #define internal nodes
        # p "##### #{@x.outputs[0].dimensions}\t#{@k.outputs[0].dimensions}"
        # addkey = XorNode.new(name:"ARK_0", inputs:[@x.outputs[0],extract_four_first(@k.outputs[0])])
        c1 = 0
        c2 = 2
        addkey = XorNode.new(name:"ARK_0", inputs:[
            @x.outputs[0],
            combine_columns_2h(
                key_nodes[(c1/6).floor].outputs[0], c1%6,
                key_nodes[(c2/6).floor].outputs[0], c2%6,
            )
        ])
        nodes.push(addkey)
        kprev = @k
        for i in 1..nb_rounds do
            c1 = (i)*4
            c2 = (i)*4 + 2
            # puts "round #{i}:\tm1:#{(c1/6).floor}\tm2:#{(c2/6).floor}"
            ki = combine_columns_2h(
                key_nodes[(c1/6).floor].outputs[0], c1%6,
                key_nodes[(c2/6).floor].outputs[0], c2%6,
            )
            # p ki
            subbytes = SubBytesNode.new(name:"SB_#{i}", input:addkey.outputs[0], subtable:@sbox)
            nodes.push(subbytes)
            shiftrows = ShiftRowsNode.new(name:"SR_#{i}", input:subbytes.outputs[0])
            nodes.push(shiftrows)
            if i < nb_rounds
                mixcolumns = MixColumnNode.new(name:"MC_#{i}", input:shiftrows.outputs[0], m:@mc)
                nodes.push(mixcolumns)
            else
                mixcolumns = shiftrows
            end
            addkey = XorNode.new(name:"ARK_#{i}", inputs:[mixcolumns.outputs[0],ki])
            nodes.push(addkey)
        end
        # define dag inputs/outputs
        super([@x,@k], [addkey], nodes)
    end

    ##
    # @param x [2DVec<u8>] input message
    # @param k [2DVec<u8>] input key
    # @returns [2DVec<u8>] crypted message
    def simulate_behavior(x,k)
        computed_outputs = compute_set_of_operators(
            x.flatten()+k.flatten(), # input values
            @x.flatten_output(0)+@k.flatten_output(0), # input variables
            @output_nodes[0].flatten_output(0), # output variables
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        return computed_outputs
    end


end