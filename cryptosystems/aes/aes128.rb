#!/usr/bin/ruby

require_relative './constants.rb'

require_relative '../../cryptodag.rb'
require_relative '../../simulate_cryptodag.rb'

require_relative '../../nodes/input.rb'
require_relative '../../nodes/xor.rb'
require_relative '../../nodes/subbytes.rb'
require_relative '../../nodes/shiftrows.rb'
require_relative '../../nodes/mixcolumn.rb'
require_relative '../../nodes/aeskeyschedule.rb'


# implements the AES 128 DAG
class AES128_Dag < CryptoDag

    def initialize(nb_rounds=10)
        # prepare constants
        @rcon = rcon
        @mc = mixcolumns_matrix()
        @sbox = s_box()
        # define input nodes
        nodes = []
        @nb_rounds = nb_rounds
        @x = InputNode.new(name:"X", dimensions:[4,4])
        nodes.push(@x)
        @k = InputNode.new(name:"K", dimensions:[4,4])
        nodes.push(@k)
        #define internal nodes
        addkey = XorNode.new(name:"ARK_0", inputs:[@x.outputs[0],@k.outputs[0]])
        nodes.push(addkey)
        kprev = @k
        for i in 1..nb_rounds do
            ki = AESKeyScheduleNode.new(name:"K_#{i}", input:kprev.outputs[0], subtable:@sbox, rcon:@rcon[i])
            nodes.push(ki)
            kprev = ki
            subbytes = SubBytesNode.new(name:"SB_#{i}", input:addkey.outputs[0], subtable:@sbox)
            nodes.push(subbytes)
            shiftrows = ShiftRowsNode.new(name:"SR_#{i}", input:subbytes.outputs[0])
            nodes.push(shiftrows)
            if i < nb_rounds
                mixcolumns = MixColumnNode.new(name:"MC_#{i}", input:shiftrows.outputs[0], m:@mc)
                nodes.push(mixcolumns)
            else
                mixcolumns = shiftrows
            end
            addkey = XorNode.new(name:"ARK_#{i}", inputs:[mixcolumns.outputs[0],ki.outputs[0]])
            nodes.push(addkey)
        end
        # define dag inputs/outputs
        super([@x,@k], [addkey], nodes)
    end

    ##
    # @param x [2DVec<u8>] input message
    # @param k [2DVec<u8>] input key
    # @returns [2DVec<u8>] crypted message
    def simulate_behavior(x,k)
        computed_outputs = compute_set_of_operators(
            x.flatten()+k.flatten(), # input values
            @x.flatten_output(0)+@k.flatten_output(0), # input variables
            @output_nodes[0].flatten_output(0), # output variables
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        return computed_outputs
    end


end