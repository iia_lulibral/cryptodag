#!/usr/bin/ruby

require_relative './constants.rb'

require_relative '../../cryptodag.rb'

require_relative '../../cryptonodes/input.rb'
require_relative '../../cryptonodes/constant.rb'
require_relative '../../cryptonodes/output.rb'
require_relative '../../cryptonodes/keyseparation.rb'
require_relative '../../cryptonodes/xor.rb'
require_relative '../../cryptonodes/subcell.rb'
require_relative '../../cryptonodes/shufflecell.rb'
require_relative '../../cryptonodes/mixcolumn.rb'


class Skinny_Dag < CryptoDag

    def initialize(nb_rounds=16)
        # # define input nodes
        # @nb_rounds = nb_rounds
        # @x = InputNode.new(4,4, name="X")
        # @k = InputNode.new(4,4, name="K")
        # @k0 = KeySeparationNode.new(input:@k, starting:0, ending:2, name:"K0")
        # @k1 = KeySeparationNode.new(input:@k, starting:2, ending:4, name:"K1")
        # @csts = []
        # for i in 0..nb_rounds-2 do
        #     cst = ConstantNode.new(4,4, name="Cst_%d"%[i])
        #     @csts.append(cst)
        # end
        # #define internal nodes
        # wk = XorNode.new([@k0,@k1], name="WKxor")
        # addkey = XorNode.new([@x,wk], name="addKey")
        # for i in 0..nb_rounds-2 do
        #     subcell = SubCellNode.new(addkey, method(:subcell_function), name="subCell_%d"%[i])
        #     shufflecell = ShuffleCellNode.new(subcell, shufflecell_matrix, name="shuffleCell_%d"%[i])
        #     mixcolumns = MixColumnNode.new(shufflecell, mixcolumns_matrix, name="mixColumns_%d"%[i])
        #     rk = XorNode.new([@csts[i], (i%2==0)? @k0 : @k1], name="roundKey_%d"%[i])
        #     addkey = XorNode.new([mixcolumns, rk], name="endRound_%d"%[i])
        # end
        # subcell = SubCellNode.new(addkey, method(:subcell_function), name="subCell_end")    
        # out_xor = XorNode.new([subcell, wk], name="xor_end")
        # # define output
        # super(OutputNode.new(out_xor, "C"))
    end

    def simulate_behavior(x,k)
        # # simulates constants
        # for i in 0..@nb_rounds-2 do
        #     @csts[i].simulate_input(cst_tabs(i))
        # end
        # # simulates x,k input
        # @x.simulate_input(x)
        # @k.simulate_input(k)
        # # recursively computes c value and returns it 
        # return @output.compute()
    end
end