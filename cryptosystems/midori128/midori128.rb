#!/usr/bin/ruby

require_relative './constants.rb'

require_relative '../../cryptodag.rb'
require_relative '../../simulate_cryptodag.rb'

require_relative '../../nodes/input.rb'
require_relative '../../nodes/indexedsubbytes.rb'
require_relative '../../nodes/shufflecell.rb'
require_relative '../../nodes/mixcolumn.rb'
require_relative '../../nodes/xor.rb'


class Midori128_Dag < CryptoDag

    def initialize(nb_rounds=20)
        # define input nodes
        nodes = []
        @nb_rounds = nb_rounds
        @x = InputNode.new(name:"X", dimensions:[4,4])
        nodes.push(@x)
        @k = InputNode.new(name:"K", dimensions:[4,4])
        nodes.push(@k)
        @csts = []
        for i in 0..nb_rounds-2 do
            cst = InputNode.new(name:"Cst_#{i}", dimensions:[4,4], values:cst_tabs(i))
            nodes.push(cst)
            @csts.append(cst)
        end
        #define internal nodes
        addkey = XorNode.new(name:"addKey", inputs:[@x.outputs[0],@k.outputs[0]])
        nodes.push(addkey)
        for i in 0..nb_rounds-2 do
            subbytes = IndexedSubBytesNode.new(name:"SB_#{i}", input:addkey.outputs[0], subtable_function: method(:get_indexed_subcell))
            nodes.push(subbytes)
            shufflecell = ShuffleCellNode.new(name:"SC_#{i}", input:subbytes.outputs[0], permutation:shufflecell_matrix)
            nodes.push(shufflecell)
            mixcolumns = MixColumnNode.new(name:"MC_#{i}", input:shufflecell.outputs[0], m:mixcolumns_matrix)
            nodes.push(mixcolumns)
            rk = XorNode.new(name:"RK_#{i}", inputs:[@csts[i].outputs[0], @k.outputs[0]])
            nodes.push(rk)
            addkey = XorNode.new(name:"End_#{i}", inputs:[mixcolumns.outputs[0],rk.outputs[0]])
            nodes.push(addkey)
        end
        subcell = IndexedSubBytesNode.new(name:"SB_#{nb_rounds-1}", input:addkey.outputs[0], subtable_function: method(:get_indexed_subcell))
        nodes.push(subcell)
        out_xor = XorNode.new(name:"End", inputs:[subcell.outputs[0], @k.outputs[0]])
        nodes.push(out_xor)
        # define dag inputs/outputs
        super([@x,@k], [out_xor], nodes)
    end

    ##
    # @param x [2DVec<u8>] input message
    # @param k [2DVec<u8>] input key
    # @returns [2DVec<u8>] crypted message
    def simulate_behavior(x,k)
        computed_outputs = compute_set_of_operators(
            x.flatten()+k.flatten(), # input values
            @x.flatten_output(0)+@k.flatten_output(0), # input variables
            @output_nodes[0].flatten_output(0), # output variables
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        return computed_outputs
    end


    # def simulate_behavior(x,k)
    #     # simulates constants
    #     for i in 0..@nb_rounds-2 do
    #         @csts[i].simulate_input(cst_tabs(i))
    #     end
    #     # simulates x,k input
    #     @x.simulate_input(x)
    #     @k.simulate_input(k)
    #     # recursively computes c value and returns it 
    #     return @output.compute()
    # end
end