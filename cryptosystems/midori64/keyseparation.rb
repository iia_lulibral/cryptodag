require_relative '../cryptodag.rb'


## KeySeparationNode Takes a key as an input and separates it into another key value
class KeySeparationNode < CryptoDagNode

    def initialize(input:,starting:,ending:,name:)
        super(x:input.x, y:input.y, inputs:[input], name:name)
        @starting = starting
        @ending = ending
    end

    def compute_call()
        input = @inputs[0].compute()
        return key_separation(input, @starting, @ending)
    end

    # separates a key matrix (k_matrix) (4x4)->[0,FF] into a k_matrix (4x4)->[0,F]
    def key_separation(k_matrix, s, e)
        row = 0
        col = 0
        res = Array.new(k_matrix.length()) { |i| Array.new(k_matrix[0].length()) { |j| 0 } }
        for i in s..e-1 do
            for j in 0..4-1
                least_significant_bits = k_matrix[j][i] % 16
                most_significant_bits = (k_matrix[j][i] ^ least_significant_bits) / 16
                raise "KeySeparationNode: key_separation, error splitting byte" unless k_matrix[j][i] == most_significant_bits*16+least_significant_bits
                if row >= 4
                    row = 0
                    col += 1
                end
                res[row][col] = most_significant_bits
                res[row+1][col] = least_significant_bits
                row += 2
            end
        end
        return res
    end

end