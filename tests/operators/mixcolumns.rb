#!/usr/bin/ruby

require_relative "../../cryptodag.rb"
require_relative "../../operators/elementary.rb"
require_relative "../../constraints/all.rb"
require_relative "../../backends/minizinc.rb"

require "minitest/autorun"

require "pry"


class TestMixColumns < Minitest::Unit::TestCase

    def test_aes()
        inputs = [
            Variable.new("in_a", 0..255),
            Variable.new("in_b", 0..255),
            Variable.new("in_c", 0..255),
            Variable.new("in_d", 0..255)
        ]
        outputs = [
            Variable.new("out_a", 0..255),
            Variable.new("out_b", 0..255),
            Variable.new("out_c", 0..255),
            Variable.new("out_d", 0..255)
        ]
        model = Model.new()
        model.add_variables(*inputs)
        model.add_variables(*outputs)
        op = MixColumnsOperator.new(inputs, outputs, [
            [2,3,1,1],
            [1,2,3,1],
            [1,1,2,3],
            [3,1,1,2]
        ])
        op.register_to_model(model)
        puts Minizinc.new.generate_code(model)
    end

end