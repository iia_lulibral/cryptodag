#!/usr/bin/ruby
## test Xor operator

require_relative "../../operators/elementary.rb"

require "minitest/autorun"
require "pry"


class TestExecXor < Minitest::Unit::TestCase

    def test_2input()
        a = Variable.new("a", 0..255)
        b = Variable.new("b", 0..255)
        c = Variable.new("c", 0..255)
        op = XorOperator.new(a,b, c)
        assert_equal(op.compute([0,0]), [0])
        assert_equal(op.compute([0,1]), [1])
        assert_equal(op.compute([1,1]), [0])
        assert_equal(op.compute([3,2]), [1])
    end

    def test_3input()
        a = Variable.new("a", 0..255)
        b = Variable.new("b", 0..255)
        c = Variable.new("c", 0..255)
        d = Variable.new("d", 0..255)
        op = XorOperator.new(a,b,c, d)
        assert_equal(op.compute([0,0,0]), [0])
        assert_equal(op.compute([0,0,1]), [1])
        assert_equal(op.compute([0,1,1]), [0])
        assert_equal(op.compute([1,1,1]), [1])
        assert_equal(op.compute([1,2,3]), [0])
    end

end