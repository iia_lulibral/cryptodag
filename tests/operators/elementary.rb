#!/usr/bin/ruby

require_relative "../../cryptodag.rb"
require_relative "../../operators/elementary.rb"

require_relative "../../constraints/all.rb"
# require_relative '../../constraints/model.rb'

require_relative "../../backends/minizinc.rb"

require "minitest/autorun"
# require "minitest/color"

require "pry"


class TestOpEq < Minitest::Unit::TestCase
  def setup
    name = "a"
    @vars = []
    10.times do |var|
      @vars << Variable.new(name.dup, 0..255)
      name.next!
    end
    @model = Model.new()
  end

  def register(opClass, arity)
    op = opClass.new(*@vars.take(arity))
    op.register_to_model(@model)
    puts Minizinc.new.generate_code(@model)
    # TODO(all) define a model and compare it instead of printing in the terminal
    # this should allow a more rigourous testing
  end

  def test_equality_operator()
    register(EqualityOperator, 2)
    assert_equal(1, @model.constraints.length)
    constraint = @model.constraints[0]
    assert_instance_of(Equality, constraint)
    assert_equal("a", constraint.v1.name.name)
    assert_equal("b", constraint.v2.name.name)
  end

  def test_xor_operator()
    register(XorOperator, 3)
    assert_equal(1, @model.constraints.length)
    constraint = @model.constraints[0]
    assert_instance_of(Equality, constraint)
    xor = constraint.v1
    assert_instance_of(Xor, xor)
    p
    assert_equal("a", xor.parameters[0].name.name)
    assert_equal("b", xor.parameters[1].name.name)
    assert_equal("c", constraint.v2.name.name)
  end
end
