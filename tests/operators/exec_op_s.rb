#!/usr/bin/ruby
## test S operator

require_relative "../../operators/elementary.rb"

require "minitest/autorun"
require "pry"


class TestExecS < Minitest::Unit::TestCase

    def test_a()
        a = Variable.new("a", 0..255)
        b = Variable.new("b", 0..255)
        op = SOperator.new(a, b, (0..255).map{|i| i*2 % 256}.to_a)
        assert_equal(op.compute([0]), [0])
        assert_equal(op.compute([1]), [2])
        assert_equal(op.compute([2]), [4])
        assert_equal(op.compute([128]), [0])
    end

end