#!/usr/bin/ruby
## test MixColumns operator

require_relative "../../operators/elementary.rb"

require "minitest/autorun"
require "pry"


class TestExecS < Minitest::Unit::TestCase

    def test_aes()
        inputs = [
            Variable.new("a", 0.255),
            Variable.new("b", 0.255),
            Variable.new("c", 0.255),
            Variable.new("d", 0.255),
        ]
        outputs = [
            Variable.new("e", 0.255),
            Variable.new("f", 0.255),
            Variable.new("g", 0.255),
            Variable.new("h", 0.255),    
        ]
        m = [
            [2,3,1,1],
            [1,2,3,1],
            [1,1,2,3],
            [3,1,1,2],
        ]
        op = MixColumnsOperator.new(inputs, outputs, m)
        assert_equal(op.compute([0,0,0,0]), [0,0,0,0])
        assert_equal(op.compute([1,1,1,1]), [1,1,1,1])
        assert_equal(op.compute([219,19,83,69]), [142,77,161,188])
        assert_equal(op.compute([242,10,34,92]), [159,220,88,157])
        assert_equal(op.compute([198,198,198,198]), [198,198,198,198])
        assert_equal(op.compute([212,212,212,213]), [213,213,215,214])
        assert_equal(op.compute([45,38,49,76]), [77,126,189,248])
    end

    def test_midori()
        inputs = [
            Variable.new("a", 0.255),
            Variable.new("b", 0.255),
            Variable.new("c", 0.255),
            Variable.new("d", 0.255),
        ]
        outputs = [
            Variable.new("e", 0.255),
            Variable.new("f", 0.255),
            Variable.new("g", 0.255),
            Variable.new("h", 0.255),    
        ]
        m = [
            [0,1,1,1],
            [1,0,1,1],
            [1,1,0,1],
            [1,1,1,0],
        ]
        op = MixColumnsOperator.new(inputs, outputs, m)
        assert_equal(op.compute([0,0,0,0]), [0,0,0,0])
        assert_equal(op.compute([1,1,1,1]), [1,1,1,1])
        assert_equal(op.compute([
            0xa5,
            0x08,
            0x10,
            0x88,
        ]),[
            0x90,
            0x3d,
            0x25,
            0xbd,           
        ])
        assert_equal(op.compute([
            0x56,
            0x2c,
            0x77,
            0xa3,
        ]),[
            0xf8,
            0x82,
            0xd9,
            0x0d,           
        ])
        assert_equal(op.compute([
            0xb8,
            0xb7,
            0xac,
            0x84,
        ]),[
            0x9f,
            0x90,
            0x8b,
            0xa3,           
        ])
        assert_equal(op.compute([
            0x3f,
            0x9f,
            0xc2,
            0x2b,
        ]),[
            0x76,
            0xd6,
            0x8b,
            0x62,           
        ])
    end

end