#!/usr/bin/ruby
## test equality operator

require_relative "../../operators/elementary.rb"

require "minitest/autorun"
require "pry"


class TestExecEq < Minitest::Unit::TestCase

    def test_a()
        a = Variable.new("a", 0..255)
        b = Variable.new("b", 0..255)
        op = EqualityOperator.new(a,b)
        assert_equal(op.compute([0]), [0])
        assert_equal(op.compute([1]), [1])
        assert_equal(op.compute([2]), [2])
    end

end