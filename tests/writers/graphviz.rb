#!/usr/bin/ruby

require_relative "../../cryptodag.rb"
require_relative "../../cryptosystems/midori128/midori128.rb"
require_relative "../../writers/graphviz_atomic.rb"

require "minitest/autorun"
# require "minitest/color"

require "pry"


class TestWriterGraphviz < Minitest::Unit::TestCase
    def test_a()
        dag = Midori128_Dag.new(nb_rounds=3)
        # p get_graphviz(dag)
        writefile_graphviz(dag.atoms(), dag.operators(), "test.dot")
        assert_equal(true,true)
    end
end