#!/usr/bin/ruby

require_relative "../../cryptodag.rb"
require_relative "../../cryptosystems/aes/aes128.rb"
require_relative "../../writers/abstract_constraints_atomic.rb"
require_relative "../../backends/minizinc"
require_relative "../../writers/graphviz_atomic.rb"

require "minitest/autorun"
# require "minitest/color"

require "pry"

class TestWriterGraphviz < Minitest::Unit::TestCase
  def test_aes128()
    # create dag
    nb_rounds = 5
    dag = AES128_Dag.new(nb_rounds = nb_rounds)
    # shave dag
    atoms,operators = *shave_dag(dag.atoms, dag.operators)
    writefile_graphviz(atoms, operators, "aes128_#{nb_rounds}.dot")
    # create initial model
    obj_values = {}
    obj_values[3] = 5
    obj_values[4] = 12
    obj_values[5] = 17
    model,variable_dict,xor_clauses = *create_abstract_model(
      atoms,
      operators,
      # "solve minimize obj",
      "constraint obj=#{obj_values[nb_rounds]}; solve satisfy",
      mds_set=Set[0,5,6,7,8]
    )
    # add diff variables
    xor_clauses = xor_clauses + add_diff_variables_mixcolumn_lines(model, atoms, operators, variable_dict, mds_set=Set[0,5,6,7,8])

    # generate new xor clauses and add them to the model
    puts("ADDING XOR CLAUSES")
    puts("\tstarting with #{xor_clauses.length} clauses")
    xor_clauses = generate_xors(atoms, operators, xor_clauses, max_size=5)
    puts("\tnow having: #{xor_clauses.length} clauses")
    xor_clauses_sizes = [0,0,0,0,0,0,0]
    xor_clauses.each do |xor|
        xor_clauses_sizes[xor.length] += 1
        model.add_constraints(Diff1.new(*(xor.map{|v| variable_dict.fetch(v,v)})))
    end
    puts("\tXOReq sizes:")
    for i in 1..6 
      puts("\t\t#{i}\t#{xor_clauses_sizes[i]}")
    end

    file = File.open("aes128_#{nb_rounds}.mzn", "w")
    file.puts(Minizinc.new.generate_code(model))
    file.close
    puts "FILE: aes128_#{nb_rounds}.mzn correctly written"
    assert_equal(true, true)
  end
end
