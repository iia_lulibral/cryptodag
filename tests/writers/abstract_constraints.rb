#!/usr/bin/ruby

require_relative "../../cryptodag.rb"
require_relative "../../cryptosystems/midori128/midori128.rb"
require_relative "../../writers/abstract_constraints_atomic.rb"
require_relative "../../backends/minizinc"
require_relative "../../writers/graphviz_atomic.rb"

require "minitest/autorun"
# require "minitest/color"

require "pry"

class TestWriterGraphviz < Minitest::Unit::TestCase
  def test_a()
    # create dag
    nb_rounds = 4
    dag = Midori128_Dag.new(nb_rounds = nb_rounds)
    # shave dag
    atoms,operators = *shave_dag(dag.atoms, dag.operators)
    writefile_graphviz(atoms, operators, "test#{nb_rounds}.dot")
    # create initial model
    model,variable_dict,xor_clauses = *create_abstract_model(
      atoms,
      operators,
      # "solve minimize obj"
      "constraint obj=#{nb_rounds}; solve satisfy"
    )
    # add diff variables
    xor_clauses = xor_clauses + add_diff_variables_mixcolumn_lines(model, atoms, operators, variable_dict)
    # add midori128 specific constraints
    mc_operators = operators.filter{|op| op.class <= MixColumnsOperator}
    mc_operators.each do |op|
    #     model.add_constraints(Diff1.new(variable_dict[op.outputs[0]], variable_dict[op.inputs[1]], variable_dict[op.inputs[2]], variable_dict[op.inputs[3]]))
    #     model.add_constraints(Diff1.new(variable_dict[op.outputs[1]], variable_dict[op.inputs[0]], variable_dict[op.inputs[2]], variable_dict[op.inputs[3]]))
    #     model.add_constraints(Diff1.new(variable_dict[op.outputs[2]], variable_dict[op.inputs[0]], variable_dict[op.inputs[1]], variable_dict[op.inputs[3]]))
    #     model.add_constraints(Diff1.new(variable_dict[op.outputs[3]], variable_dict[op.inputs[0]], variable_dict[op.inputs[1]], variable_dict[op.inputs[2]]))
        xor_clauses.push(Set.new([variable_dict[op.outputs[0]], variable_dict[op.inputs[1]], variable_dict[op.inputs[2]], variable_dict[op.inputs[3]]]))
        xor_clauses.push(Set.new([variable_dict[op.outputs[1]], variable_dict[op.inputs[0]], variable_dict[op.inputs[2]], variable_dict[op.inputs[3]]]))
        xor_clauses.push(Set.new([variable_dict[op.outputs[2]], variable_dict[op.inputs[0]], variable_dict[op.inputs[1]], variable_dict[op.inputs[3]]]))
        xor_clauses.push(Set.new([variable_dict[op.outputs[3]], variable_dict[op.inputs[0]], variable_dict[op.inputs[1]], variable_dict[op.inputs[2]]]))
    end
    # generate new xor clauses and add them to the model
    puts("ADDING XOR CLAUSES")
    puts("\tstarting with #{xor_clauses.length} clauses")
    xor_clauses = generate_xors(atoms, operators, xor_clauses)
    puts("\tnow having: #{xor_clauses.length} clauses")
    xor_clauses.each do |xor|
        model.add_constraints(Diff1.new(*(xor.map{|v| variable_dict.fetch(v,v)})))
    end

    file = File.open("test#{nb_rounds}.mzn", "w")
    file.puts(Minizinc.new.generate_code(model))
    file.close
    assert_equal(true, true)
  end
end
