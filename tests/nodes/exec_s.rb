#!/usr/bin/ruby
## test XorNode

require_relative "../../nodes/input.rb"
require_relative "../../nodes/subbytes.rb"
require_relative "../../simulate_cryptodag.rb"

require "minitest/autorun"
require "pry"


class TestExecSNode < Minitest::Unit::TestCase
    def test_simple22s()
        subtable_1 = 256.times.map{|i|(i*2) % 256}.to_a
        subtable_2 = 256.times.map{|i|(i+1) % 256}.to_a
        a_node = InputNode.new(name:"input_a",dimensions:[2,2])
        s1_node = SubBytesNode.new(name:"s1", input:a_node.outputs[0], subtable:subtable_1)
        s2_node = SubBytesNode.new(name:"s2", input:s1_node.outputs[0], subtable:subtable_2)
        computed_outputs = compute_set_of_operators(
            [0,1,2,3], # input values
            a_node.flatten_output(0), # input variables
            s2_node.flatten_output(0), # output variables
            s1_node.operators+s2_node.operators
        )
        assert_equal(computed_outputs, [1,3,5,7])
    end

end