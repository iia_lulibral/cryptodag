#!/usr/bin/ruby
## test InputNode

require_relative "../../nodes/input.rb"
require_relative "../../simulate_cryptodag.rb"

require "minitest/autorun"
require "pry"


class TestExecInputNode < Minitest::Unit::TestCase
    def test_22input()
        node = InputNode.new(name:"input1",dimensions:[2,2])
        computed_outputs = compute_set_of_operators(
            [1,2,3,4],
            [node.outputs[0][0][0],node.outputs[0][0][1],node.outputs[0][1][0],node.outputs[0][1][1]],
            [node.outputs[0][0][0],node.outputs[0][0][1],node.outputs[0][1][0],node.outputs[0][1][1]],
            node.operators
        ) 
        assert_equal(computed_outputs, [1,2,3,4])
    end
end