#!/usr/bin/ruby
## test XorNode

require_relative "../../nodes/input.rb"
require_relative "../../nodes/xor.rb"
require_relative "../../simulate_cryptodag.rb"

require "minitest/autorun"
require "pry"


class TestExecXorNode < Minitest::Unit::TestCase
    def test_simple22xor()
        a_node = InputNode.new(name:"input_a",dimensions:[2,2])
        b_node = InputNode.new(name:"input_b",dimensions:[2,2])
        xor_node = XorNode.new(name:"xor", inputs:[a_node.outputs[0], b_node.outputs[0]])
        assert_equal(xor_node.operators.length, 4)
        assert_equal(xor_node.outputs.length, 1)
        assert_equal(xor_node.flatten_input(0).length, 4)
        assert_equal(xor_node.flatten_output(0).length, 4)
        computed_outputs = compute_set_of_operators(
            [1,2,3,4]+[0,1,3,4], # input values
            a_node.flatten_output(0)+b_node.flatten_output(0), # input variables
            xor_node.flatten_output(0), # output variables
            xor_node.operators
        )
        assert_equal(computed_outputs, [1,3,0,0])
    end

    def test_double22xor()
        a_node = InputNode.new(name:"input_a",dimensions:[2,2])
        b_node = InputNode.new(name:"input_b",dimensions:[2,2])
        c_node = XorNode.new(name:"xor_c", inputs:[a_node.outputs[0], b_node.outputs[0]])
        d_node = InputNode.new(name:"input_d", dimensions:[2,2])
        e_node = XorNode.new(name:"xor_e", inputs:[c_node.outputs[0], d_node.outputs[0]])
        computed_outputs = compute_set_of_operators(
            [1,2,3,4]+[0,1,3,4]+[0,2,1,1], # input values
            a_node.flatten_output(0)+b_node.flatten_output(0)+d_node.flatten_output(0), # input variables
            e_node.flatten_output(0), # output variables
            e_node.operators+c_node.operators
        )
        assert_equal(computed_outputs, [1,1,1,1])
    end

    def test_triple22xor()
        a_node = InputNode.new(name:"input_a",dimensions:[2,2])
        b_node = InputNode.new(name:"input_b",dimensions:[2,2])
        c_node = XorNode.new(name:"xor_c", inputs:[a_node.outputs[0], b_node.outputs[0]])
        d_node = InputNode.new(name:"input_d", dimensions:[2,2])
        e_node = XorNode.new(name:"xor_e", inputs:[c_node.outputs[0], d_node.outputs[0]])
        f_node = InputNode.new(name:"input_f", dimensions:[2,2])
        g_node = XorNode.new(name:"xor_g", inputs:[e_node.outputs[0], f_node.outputs[0]])
        computed_outputs = compute_set_of_operators(
            [1,2,3,4]+[0,1,3,4]+[0,2,1,1]+[1,1,1,1], # input values
            a_node.flatten_output(0)+b_node.flatten_output(0)+d_node.flatten_output(0)+f_node.flatten_output(0), # input variables
            g_node.flatten_output(0), # output variables
            e_node.operators+c_node.operators+g_node.operators
        )
        assert_equal(computed_outputs, [0,0,0,0])
    end

end