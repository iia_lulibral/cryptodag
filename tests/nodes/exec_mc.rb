#!/usr/bin/ruby
## test XorNode

require_relative "../../nodes/input.rb"
require_relative "../../nodes/mixcolumn.rb"
require_relative "../../simulate_cryptodag.rb"

require "minitest/autorun"
require "pry"


class TestExecSNode < Minitest::Unit::TestCase
    def test_simple22s()
        a_node = InputNode.new(name:"input_a",dimensions:[4,4])
        mc_node = MixColumnNode.new(name:"mc", input:a_node.outputs[0], m:[
            [2,3,1,1],
            [1,2,3,1],
            [1,1,2,3],
            [3,1,1,2],
        ]) # AES mix columns
        computed_outputs = compute_set_of_operators(
            [
                0,  1,  219 ,242,
                0,  1,  19  ,10,
                0,  1,  83  ,34,
                0,  1,  69  ,92
            ].flatten, # input values
            a_node.flatten_output(0), # input variables
            mc_node.flatten_output(0), # output variables
            mc_node.operators
        )
        assert_equal(computed_outputs, [
            0,  1,  142 ,159,
            0,  1,  77  ,220,
            0,  1,  161 ,88,
            0,  1,  188 ,157,
        ].flatten)
    end

end