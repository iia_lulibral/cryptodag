#!/usr/bin/ruby
## test AESKeyScheduleNode
require_relative "../../nodes/input.rb"
require_relative "../../nodes/aeskeyschedule.rb"
require_relative "../../simulate_cryptodag.rb"
require_relative "../../cryptosystems/aes/constants.rb"

require "minitest/autorun"
require "pry"


class TestExecAesKeySchedule < Minitest::Unit::TestCase

    def setup()
        @sbox = [
            0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
            0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
            0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
            0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
            0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
            0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
            0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
            0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
            0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
            0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
            0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
            0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
            0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
            0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
            0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
            0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
        ]
    end

    def test_a()
        k = InputNode.new(name:"K0",dimensions:[4,4])
        ks = AESKeyScheduleNode.new(name:"KS", input:k.outputs[0], subtable:@sbox, rcon:1)
        computed_outputs = compute_set_of_operators(
            [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
            ],
            ks.operators
        )
        assert_equal(computed_outputs, [
            0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63
        ])
    end

    def test_b()
        k = InputNode.new(name:"K0",dimensions:[4,4])
        ks = AESKeyScheduleNode.new(name:"KS", input:k.outputs[0], subtable:@sbox, rcon:2)
        computed_outputs = compute_set_of_operators(
            [0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
            ],
            ks.operators
        )
        assert_equal(computed_outputs, [
            0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa, 0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa 
        ])
    end

    def test_chain_aes128()
        k = InputNode.new(name:"K0",dimensions:[4,4])
        ks1 = AESKeyScheduleNode.new(name:"KS1", input:k.outputs[0], subtable:@sbox,   rcon:rcon[1])
        ks2 = AESKeyScheduleNode.new(name:"KS2", input:ks1.outputs[0], subtable:@sbox, rcon:rcon[2])
        computed_outputs = compute_set_of_operators(
            [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
            ],
            [
                ks2.outputs[0][0][0],ks2.outputs[0][1][0],ks2.outputs[0][2][0],ks2.outputs[0][3][0],
                ks2.outputs[0][0][1],ks2.outputs[0][1][1],ks2.outputs[0][2][1],ks2.outputs[0][3][1],
                ks2.outputs[0][0][2],ks2.outputs[0][1][2],ks2.outputs[0][2][2],ks2.outputs[0][3][2],
                ks2.outputs[0][0][3],ks2.outputs[0][1][3],ks2.outputs[0][2][3],ks2.outputs[0][3][3],
            ],
            ks1.operators+ks2.operators
        )
        assert_equal(computed_outputs, [
            0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa, 0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa 
        ])
    end

    def test_aes192()
        k = InputNode.new(name:"K0",dimensions:[4,6])
        ks = AESKeyScheduleNode.new(name:"KS", input:k.outputs[0], subtable:@sbox, rcon:rcon[1])
        computed_outputs = compute_set_of_operators(
            [255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
                k.outputs[0][0][4],k.outputs[0][1][4],k.outputs[0][2][4],k.outputs[0][3][4],
                k.outputs[0][0][5],k.outputs[0][1][5],k.outputs[0][2][5],k.outputs[0][3][5],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
                ks.outputs[0][0][4],ks.outputs[0][1][4],ks.outputs[0][2][4],ks.outputs[0][3][4],
                ks.outputs[0][0][5],ks.outputs[0][1][5],ks.outputs[0][2][5],ks.outputs[0][3][5],
            ],
            ks.operators
        )
        assert_equal(computed_outputs, [
            0xe8, 0xe9, 0xe9, 0xe9,
            0x17, 0x16, 0x16, 0x16, 
            0xe8, 0xe9, 0xe9, 0xe9,
            0x17, 0x16, 0x16, 0x16,
            0xe8, 0xe9, 0xe9, 0xe9,
            0x17, 0x16, 0x16, 0x16
        ])
    end

    def test_simple_aes192_2()
        k = InputNode.new(name:"K0",dimensions:[4,6])
        ks = AESKeyScheduleNode.new(name:"KS", input:k.outputs[0], subtable:@sbox, rcon:rcon[2])
        computed_outputs = compute_set_of_operators(
            [   
                232,	233,	233,	233,	23,	22,	22,	22,	232,	233,	233,	233,	23,	22,	22,	22,	232,	233,	233,	233,	23,	22,	22,	22
            ],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
                k.outputs[0][0][4],k.outputs[0][1][4],k.outputs[0][2][4],k.outputs[0][3][4],
                k.outputs[0][0][5],k.outputs[0][1][5],k.outputs[0][2][5],k.outputs[0][3][5],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
                ks.outputs[0][0][4],ks.outputs[0][1][4],ks.outputs[0][2][4],ks.outputs[0][3][4],
                ks.outputs[0][0][5],ks.outputs[0][1][5],ks.outputs[0][2][5],ks.outputs[0][3][5],
            ],
            ks.operators
        )
        assert_equal(computed_outputs, [
            173,	174,	174,	25,	186,	184,	184,	15,	82,	81,	81,	230,	69,	71,	71,	240,	173,	174,	174,	25,	186,	184,	184,	15
        ])
    end

    def test_aes192_2()
        k = InputNode.new(name:"K0",dimensions:[4,6])
        ks0 = AESKeyScheduleNode.new(name:"KS0", input:k.outputs[0], subtable:@sbox,  rcon:rcon[1])
        ks = AESKeyScheduleNode.new(name:"KS1", input:ks0.outputs[0], subtable:@sbox, rcon:rcon[2])
        computed_outputs = compute_set_of_operators(
            [255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
                k.outputs[0][0][4],k.outputs[0][1][4],k.outputs[0][2][4],k.outputs[0][3][4],
                k.outputs[0][0][5],k.outputs[0][1][5],k.outputs[0][2][5],k.outputs[0][3][5],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
                ks.outputs[0][0][4],ks.outputs[0][1][4],ks.outputs[0][2][4],ks.outputs[0][3][4],
                ks.outputs[0][0][5],ks.outputs[0][1][5],ks.outputs[0][2][5],ks.outputs[0][3][5],
            ],
            ks0.operators+ks.operators
        )
        assert_equal(computed_outputs, [
            173,	174,	174,	25,	186,	184,	184,	15,	82,	81,	81,	230,	69,	71,	71,	240,	173,	174,	174,	25,	186,	184,	184,	15
        ])
    end

    def test_aes192_3()
        k = InputNode.new(name:"K0",dimensions:[4,6])
        ks0 = AESKeyScheduleNode.new(name:"KS0", input:k.outputs[0], subtable:@sbox, rcon:1)
        ks1 = AESKeyScheduleNode.new(name:"KS1", input:ks0.outputs[0], subtable:@sbox, rcon:2)
        ks = AESKeyScheduleNode.new(name:"KS2", input:ks1.outputs[0], subtable:@sbox, rcon:4)
        computed_outputs = compute_set_of_operators(
            [255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
                k.outputs[0][0][4],k.outputs[0][1][4],k.outputs[0][2][4],k.outputs[0][3][4],
                k.outputs[0][0][5],k.outputs[0][1][5],k.outputs[0][2][5],k.outputs[0][3][5],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
                ks.outputs[0][0][4],ks.outputs[0][1][4],ks.outputs[0][2][4],ks.outputs[0][3][4],
                ks.outputs[0][0][5],ks.outputs[0][1][5],ks.outputs[0][2][5],ks.outputs[0][3][5],
            ],
            ks.operators+ks0.operators+ks1.operators
        )
        assert_equal(computed_outputs, [
            197,	194,	216,	237,	127,	122,	96,	226,	45,	43,	49,	4,	104,	108,	118,	244,	197,	194,	216,	237,	127,	122,	96,	226
        ])
    end

    def test_aes256()
        k = InputNode.new(name:"K0",dimensions:[4,8])
        ks = AESKeyScheduleNode.new(name:"KS", input:k.outputs[0], subtable:@sbox, rcon:1)
        computed_outputs = compute_set_of_operators(
            [255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255],
            [
                k.outputs[0][0][0],k.outputs[0][1][0],k.outputs[0][2][0],k.outputs[0][3][0],
                k.outputs[0][0][1],k.outputs[0][1][1],k.outputs[0][2][1],k.outputs[0][3][1],
                k.outputs[0][0][2],k.outputs[0][1][2],k.outputs[0][2][2],k.outputs[0][3][2],
                k.outputs[0][0][3],k.outputs[0][1][3],k.outputs[0][2][3],k.outputs[0][3][3],
                k.outputs[0][0][4],k.outputs[0][1][4],k.outputs[0][2][4],k.outputs[0][3][4],
                k.outputs[0][0][5],k.outputs[0][1][5],k.outputs[0][2][5],k.outputs[0][3][5],
                k.outputs[0][0][6],k.outputs[0][1][6],k.outputs[0][2][6],k.outputs[0][3][6],
                k.outputs[0][0][7],k.outputs[0][1][7],k.outputs[0][2][7],k.outputs[0][3][7],
            ],
            [
                ks.outputs[0][0][0],ks.outputs[0][1][0],ks.outputs[0][2][0],ks.outputs[0][3][0],
                ks.outputs[0][0][1],ks.outputs[0][1][1],ks.outputs[0][2][1],ks.outputs[0][3][1],
                ks.outputs[0][0][2],ks.outputs[0][1][2],ks.outputs[0][2][2],ks.outputs[0][3][2],
                ks.outputs[0][0][3],ks.outputs[0][1][3],ks.outputs[0][2][3],ks.outputs[0][3][3],
                ks.outputs[0][0][4],ks.outputs[0][1][4],ks.outputs[0][2][4],ks.outputs[0][3][4],
                ks.outputs[0][0][5],ks.outputs[0][1][5],ks.outputs[0][2][5],ks.outputs[0][3][5],
                ks.outputs[0][0][6],ks.outputs[0][1][6],ks.outputs[0][2][6],ks.outputs[0][3][6],
                ks.outputs[0][0][7],ks.outputs[0][1][7],ks.outputs[0][2][7],ks.outputs[0][3][7],
            ],
            ks.operators
        )
        assert_equal(computed_outputs, [
            0xe8, 0xe9, 0xe9, 0xe9, 0x17, 0x16, 0x16, 0x16, 0xe8, 0xe9, 0xe9, 0xe9, 0x17, 0x16, 0x16, 0x16, 
            0x0f, 0xb8, 0xb8, 0xb8, 0xf0, 0x47, 0x47, 0x47, 0x0f, 0xb8, 0xb8, 0xb8, 0xf0, 0x47, 0x47, 0x47,
        ])
    end
end