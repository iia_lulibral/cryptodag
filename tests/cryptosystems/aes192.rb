#!/usr/bin/ruby

require 'minitest/autorun'

require_relative "../../cryptosystems/aes/aes192.rb"

class TestAES192 < Minitest::Unit::TestCase
    def test_construction()
        # create DAG
        dag = AES192_Dag.new()
        # # simulate behavior
        output = dag.simulate_behavior(
            [ # M
                [0xff, 0xff, 0xff, 0xff],
                [0xff, 0xff, 0xff, 0xff],
                [0xff, 0xff, 0xff, 0xff],
                [0xff, 0xff, 0xff, 0xff]
            ],
            [ # K
                [0xff, 0xff, 0xff, 0xff, 0xff, 0xff],
                [0xff, 0xff, 0xff, 0xff, 0xff, 0xff],
                [0xff, 0xff, 0xff, 0xff, 0xff, 0xff],
                [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
            ]
        )
        assert_equal(output, [
            191, 41, 228, 187, 112, 255, 141, 129, 3, 113, 223, 116, 78, 142, 54, 239
        ])
    end
end