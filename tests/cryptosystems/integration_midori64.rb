#!/usr/bin/ruby

require 'minitest/autorun'

require_relative "../../cryptosystems/midori64/midori64.rb"


class TestMidori64 < Minitest::Unit::TestCase
    def test_a()
        # create DAG
        dag = Midori64_Dag.new()
        # simulate behavior
        output = dag.simulate_behavior(
            [
                [0x4,0x0,0xb,0x8],
                [0x2,0xf,0x5,0x7],
                [0xc,0xd,0x8,0x9],
                [0x2,0x3,0x6,0xe]
            ],
            [
                [0x68,0x3c,0x5b,0x3e],
                [0x7d,0x85,0x10,0x2a],
                [0xed,0xb3,0x09,0x8c],
                [0x3b,0xf3,0x86,0xbf]
            ]
        )
        assert_equal(output, [
            [6, 13, 7, 0],
            [6,	12, 0, 1],
            [11, 6, 13, 12],
            [12, 2, 9, 13]
        ])
    end
end
