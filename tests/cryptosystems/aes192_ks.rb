#!/usr/bin/ruby

require 'minitest/autorun'

require_relative '../../cryptosystems/aes/aes192.rb'
require_relative '../../cryptosystems/aes/constants.rb'
require_relative '../../cryptodag.rb'
require_relative '../../simulate_cryptodag.rb'

require_relative '../../nodes/input.rb'
require_relative '../../nodes/xor.rb'
require_relative '../../nodes/subbytes.rb'
require_relative '../../nodes/shiftrows.rb'
require_relative '../../nodes/mixcolumn.rb'
require_relative '../../nodes/aeskeyschedule.rb'


class TestAES192 < Minitest::Unit::TestCase

    def setup()
        @rcon = rcon
        @mc = mixcolumns_matrix()
        @sbox = s_box()
        # define input nodes
        @nodes = []
        @nb_rounds = 12
        @x = InputNode.new(name:"X", dimensions:[4,4])
        @nodes.push(@x)
        @k = InputNode.new(name:"K", dimensions:[4,6])
        @nodes.push(@k)
        @key_nodes = []
        @key_nodes.push(@k)
        for i in  0..(@nb_rounds/1.5).ceil
            if i > 0
                k_current = AESKeyScheduleNode.new(
                    name:"K_#{i}",
                    input:@key_nodes[@key_nodes.length-1].outputs[0],
                    subtable:@sbox,
                    rcon:@rcon[i]
                )
                @key_nodes.push(k_current)
                @nodes.push(k_current)
            end
        end
        @input_values = [ # M
            [0xff, 0xff, 0xff, 0xff],
            [0xff, 0xff, 0xff, 0xff],
            [0xff, 0xff, 0xff, 0xff],
            [0xff, 0xff, 0xff, 0xff]
        ].flatten + [ # K
            [0xff, 0xff, 0xff, 0xff, 0xff, 0xff],
            [0xff, 0xff, 0xff, 0xff, 0xff, 0xff],
            [0xff, 0xff, 0xff, 0xff, 0xff, 0xff],
            [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
        ].flatten
    end

    def get_round_key(i)
        c1 = (i)*4
        c2 = (i)*4 + 2
        return combine_columns_2h(
            @key_nodes[(c1/6).floor].outputs[0], c1%6,
            @key_nodes[(c2/6).floor].outputs[0], c2%6,
        )
    end

    def test_roundkey0()
        output_atoms = get_round_key(0)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff	,0xff])
    end

    def test_roundkey1()
        output_atoms = get_round_key(1)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0xff	,0xff	,0xe8	,0x17	,0xff	,0xff	,0xe9	,0x16	,0xff	,0xff	,0xe9	,0x16	,0xff	,0xff	,0xe9	,0x16])
    end

    def test_roundkey2()
        output_atoms = get_round_key(2)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0xe8	,0x17	,0xe8	,0x17	,0xe9	,0x16	,0xe9	,0x16	,0xe9	,0x16	,0xe9	,0x16	,0xe9	,0x16	,0xe9	,0x16])
    end

    def test_roundkey3()
        output_atoms = get_round_key(3)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0xad	,0xba	,0x52	,0x45	,0xae	,0xb8	,0x51	,0x47	,0xae	,0xb8	,0x51	,0x47	,0x19	,0xf	,0xe6	,0xf0])
    end

    def test_roundkey4()
        output_atoms = get_round_key(4)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0xad	,0xba	,0xc5	,0x7f	,0xae	,0xb8	,0xc2	,0x7a	,0xae	,0xb8	,0xd8	,0x60	,0x19	,0xf	,0xed	,0xe2])
    end

    def test_roundkey5()
        output_atoms = get_round_key(5)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0x2d	,0x68	,0xc5	,0x7f	,0x2b	,0x6c	,0xc2	,0x7a	,0x31	,0x76	,0xd8	,0x60	,0x4	,0xf4	,0xed	,0xe2])
    end

    def test_roundkey6()
        output_atoms = get_round_key(6)
        computed_outputs = compute_set_of_operators(
            @input_values,
            @x.flatten_output(0)+@k.flatten_output(0),
            output_atoms,
            @nodes.map{|n| n.operators}.reduce([], :+)
        )
        assert_equal(computed_outputs,
        [0x17	,0x68	,0x45	,0x2d	,0x12	,0x68	,0x43	,0x2f	,0x40	,0x20	,0x11	,0x67	,0x3f	,0xdd	,0xd9	,0x2d])
    end
end


# prepare constants
