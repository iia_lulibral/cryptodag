#!/usr/bin/ruby

require 'minitest/autorun'

require_relative '../../cryptosystems/midori128/constants.rb'

require_relative '../../cryptodag.rb'
require_relative '../../simulate_cryptodag.rb'
require_relative '../../nodes/input.rb'
require_relative '../../nodes/indexedsubbytes.rb'
require_relative '../../nodes/shufflecell.rb'
require_relative '../../nodes/mixcolumn.rb'
require_relative '../../nodes/xor.rb'


class TestMidori128StepByStep < Minitest::Unit::TestCase

    def setup()
        @x_val = [
            0x51,0xe7,0xec,0xbc,
            0x08,0x3a,0x87,0x29,
            0x4c,0x5c,0xd7,0x75,
            0xe6,0xa2,0xba,0x43
        ]
        @k_val =  [
            0x68,0x3c,0x5b,0x3e,
            0x7d,0x85,0x10,0x2a,
            0xed,0xb3,0x09,0x8c,
            0x3b,0xf3,0x86,0xbf
        ]
        # create DAG
        @nb_rounds = 20
        @x = InputNode.new(name:"X", dimensions:[4,4])
        @k = InputNode.new(name:"K", dimensions:[4,4])
        @csts = []
        for i in 0..@nb_rounds-2 do
            cst = InputNode.new(name:"Cst_#{i}", dimensions:[4,4], values:cst_tabs(i))
            @csts.append(cst)
        end
    end

    def test_add_key()
        #define internal nodes
        nodes = []
        addkey = XorNode.new(name:"addKey", inputs:[@x.outputs[0],@k.outputs[0]])
        nodes.push(addkey)

        computed_outputs = compute_set_of_operators(
            @x_val.flatten()+@k_val.flatten(), # input values
            @x.flatten_output(0)+@k.flatten_output(0), # input variables
            addkey.flatten_output(0), # output variables
            nodes.map{|n| n.operators}.reduce([], :+)
        )

        expected_output = [
            0x39, 0xdb, 0xb7, 0x82, 
            0x75, 0xbf, 0x97, 0x03, 
            0xa1, 0xef, 0xde, 0xf9, 
            0xdd, 0x51, 0x3c, 0xfc 
        ]

        assert_equal(expected_output, computed_outputs)
    end

    def test_first_sbox()
        #define internal nodes
        nodes = []
        addkey = XorNode.new(name:"addKey", inputs:[@x.outputs[0],@k.outputs[0]])
        nodes.push(addkey)
        i=0
        subbytes = IndexedSubBytesNode.new(name:"SB_#{i}", input:addkey.outputs[0], subtable_function: method(:get_indexed_subcell))
        nodes.push(subbytes)

        computed_outputs = compute_set_of_operators(
            @x_val.flatten()+@k_val.flatten(), # input values
            @x.flatten_output(0)+@k.flatten_output(0), # input variables
            subbytes.flatten_output(0), # output variables
            nodes.map{|n| n.operators}.reduce([], :+)
        )

        expected_output = [
            0x2b, 0xe2, 0x36, 0xd5, 
            0xe0, 0xb3, 0x8f, 0xcb, 
            0xf7, 0xe9, 0xde, 0x9f, 
            0x88, 0xb3, 0x52, 0xe8, 
        ]

        assert_equal(expected_output, computed_outputs)
    end

    def test_first_shuffle()
        #define internal nodes
        nodes = []
        addkey = XorNode.new(name:"addKey", inputs:[@x.outputs[0],@k.outputs[0]])
        nodes.push(addkey)
        i=0
        subbytes = IndexedSubBytesNode.new(name:"SB_#{i}", input:addkey.outputs[0], subtable_function: method(:get_indexed_subcell))
        nodes.push(subbytes)
        shufflecell = ShuffleCellNode.new(name:"SC_#{i}", input:subbytes.outputs[0], permutation:shufflecell_matrix)
        nodes.push(shufflecell)

        computed_outputs = compute_set_of_operators(
            @x_val.flatten()+@k_val.flatten(), # input values
            @x.flatten_output(0)+@k.flatten_output(0), # input variables
            shufflecell.flatten_output(0), # output variables
            nodes.map{|n| n.operators}.reduce([], :+)
        )

        expected_output = [
            0x2b, 0x9f, 0x8f, 0xb3, 
            0xde, 0xe2, 0x88, 0xcb, 
            0xb3, 0x52, 0xd5, 0xf7, 
            0xe8, 0xe0, 0xe9, 0x36,             
        ]

        assert_equal(expected_output, computed_outputs)
    end
end