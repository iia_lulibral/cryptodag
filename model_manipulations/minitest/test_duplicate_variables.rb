require "minitest/autorun"
require "pry"

require_relative "../detect_duplicate_variables"

class TestDuplicates < MiniTest::Unit::TestCase
  def setup
    @model = Model.new

    vars = (0..3).to_a.map { |i| Variable.new("v_#{i}", "int") }
    @model.add_variables(*vars)
    @model.add_constraints(
      Equality.new(vars[0], vars[1]),
      Equality.new(vars[0], vars[2]),
      Equality.new(Sum.new(*vars), Sum.new(*vars))
    )
    @tableModel = Model.new
    @t = DTable.new("plouf", "int", [2, 2])
    @tableModel.add_variables(@t)
    vs = @t.variables
    @tableModel.add_variables(*vs)

    @tableModel.add_constraints(
      Equality.new(@t[0, 0], @t[0, 1]),
      Equality.new(@t[0, 0], @t[1, 1]),
      Equality.new(@t[1, 1], @t[1, 1]),
      Equality.new(Sum.new(*vs), Sum.new(*vs))
    )
  end

  def test_variable_substitution()
    v = @model.variables

    v[1].be_replaced_by(v[0])

    assert_match("+( v_0 , v_0 , v_2 , v_3 ) == +( v_0 , v_0 , v_2 , v_3 )",
                 @model.constraints[2].to_s)
  end

  def test_variable_substitution_complex
    simplify = Simplificator.new(@tableModel)
    simplify.remove_duplicate_variables
    assert_match("+( plouf_0_0 , plouf_0_0 , plouf_1_0 , plouf_0_0 ) == +( plouf_0_0 , plouf_0_0 , plouf_1_0 , plouf_0_0 )",
                 @tableModel.constraints.join("\n"))
  end

  def test_detects_duplicataes
    simplify = Simplificator.new(@model)
    simplify.remove_duplicate_variables
    assert_match("+( v_0 , v_0 , v_0 , v_3 ) == +( v_0 , v_0 , v_0 , v_3 )",
                 @model.constraints.join("\n"))
  end
end
