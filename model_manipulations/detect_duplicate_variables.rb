require "union_find"
require "set"
require_relative "../constraints/all"

unless UnionFind::UnionFind.public_method_defined?(:get_root)
  class UnionFind::UnionFind
    def get_root(component)
      find_root(component)
    end
  end
end

class Simplificator
  def initialize(model)
    @model = model
    @union_find = nil
    @aliasing_constraints = []
    @byRoot = Hash.new { |hash, key| hash[key] = [] }

    find_duplicate_variables()
  end

  def remove_duplicate_variables()
    @byRoot.each_pair do |root, targets|
      targets.each do |target|
        target.be_replaced_by(root) if target != root
      end
    end
    @model.constraints.reject! { |e| @aliasing_constraints.include?(e) }
  end

  protected def find_duplicate_variables()
    vars = @model.variables.to_set

    @union_find = UnionFind::UnionFind.new(vars)

    find_equalities

    vars.each do |v|
      @byRoot[@union_find.get_root(v)].push(v)
    end
    vars
  end

  def find_equalities
    @model.constraints.select { |c| c.instance_of?(Equality) }.each do |c|
      if c.v1.kind_of?(Variable) && c.v2.kind_of?(Variable) &&
         !c.v1.kind_of?(VariableInTable) && !c.v2.kind_of?(VariableInTable)
        #$stderr.puts("#{c.v1.class} == #{c.v2.class}")
        @union_find.union(c.v1, c.v2)
        @aliasing_constraints << c
      end
    end
  end
end
