#!/usr/bin/ruby

require_relative "options"

require_relative "./util"
require_relative "./cryptodag"
require_relative "./cryptosystems/midori64/midori64"
require_relative "./cryptosystems/midori128/midori128"
require_relative "writers/graphviz"
require_relative "writers/step1opt"
require_relative "writers/constraintwriter"
require_relative "writers/step1diff"
require_relative "writers/midori128replica"

require_relative "backends/minizinc"

def main
	# create DAG
	nb_rounds = 4
	dag = Midori128_Dag.new(nb_rounds = nb_rounds)

	# visualize DAG
	# print_graphviz(dag)
	writefile_graphviz(dag, "tmp/midori128.dot")

	if $options[:countdown]
    require_relative "constraints/model"
    
    if $options[:optimize]
      require_relative "model_manipulations/detect_duplicate_variables"
      simplify = Simplificator.new(model)
      simplify.remove_duplicate_variables
    end

		# model = Step1OptConstraintWriter.new.generate_model(dag)
		# model = Step1Diff.new.generate_extended_model(dag, nb_rounds)
		model = Midori128Replica.new.generate_extended_model(dag, nb_rounds)
		puts Minizinc.new.generate_code(model)
	else

		# write minizinc
		print_minizinc_dk_step1opt(dag, mc_improvements = false)

		# simulate behavior
		# output = dag.simulate_behavior(
		#     [
		#         [0x51,0xe7,0xec,0xbc],
		#         [0x08,0x3a,0x87,0x29],
		#         [0x4c,0x5c,0xd7,0x75],
		#         [0xe6,0xa2,0xba,0x43]
		#     ],
		#     [
		#         [0x68,0x3c,0x5b,0x3e],
		#         [0x7d,0x85,0x10,0x2a],
		#         [0xed,0xb3,0x09,0x8c],
		#         [0x3b,0xf3,0x86,0xbf]
		#     ]
		# )
		# nodes = dag.find_all_nodes()
		# nodes.each do |n|
		#     if n.class.name != "ConstantNode"
		#         puts "=== #{n.name} ==="
		#         matrix_pretty_print(n.compute())
		#     end
		# end
	end
end

main
