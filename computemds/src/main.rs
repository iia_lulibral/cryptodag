
mod generatexors;
use generatexors::{XorClause, Variable, generate_clauses, clause_from_vec};


pub struct BoolVec {
    pub a:bool,
    pub b:bool,
    pub c:bool,
    pub d:bool,
}

impl BoolVec {
    pub fn toint(&self) -> usize {  // result ranges in [0;15]
        return (self.a as usize)*8+(self.b as usize)*4+(self.c as usize)*2+(self.d as usize);
    }

    pub fn activebits(&self) -> usize {
        return (self.a as usize)+(self.b as usize)+(self.c as usize)+(self.d as usize);
    }
}

#[derive(Debug)]
pub struct ByteVec {
    pub a:u8,
    pub b:u8,
    pub c:u8,
    pub d:u8,
}

impl ByteVec {
    pub fn new(a:u8,b:u8,c:u8,d:u8) -> Self {
        Self {a,b,c,d}
    }

    /**
     * takes two AbstractVec 
     */
    pub fn toboolvec(&self) -> BoolVec {
        BoolVec {
            a:self.a > 0,
            b:self.b > 0,
            c:self.c > 0,
            d:self.d > 0,
        }
    }
}


pub fn computemds(op:impl Fn(&ByteVec) -> ByteVec) -> Vec<usize> {
    let mut res = vec![0;1<<8]; // array with 2^8 values (all set to 0)
    let mut nbcomputations:usize = 0;
    for a in 0..256 {
        for b in 0..256 {
            for c in 0..256 {
                for d in 0..256 {
                    nbcomputations += 1;
                    let input = ByteVec {
                        a:a as u8,
                        b:b as u8,
                        c:c as u8,
                        d:d as u8
                    };
                    // compute operation result
                    let output = op(&input);
                    if input.toboolvec().activebits()+output.toboolvec().activebits() <= 4 && input.toboolvec().activebits()+output.toboolvec().activebits() > 0 {
                        panic!("too few abstract bits in input: {:?} and output {:?}",input, output);
                    }
                    let index:usize = input.toboolvec().toint()*16 + output.toboolvec().toint();
                    if index > (1<<8)-1 {
                        panic!("index ({}) is too large to be contained in a {} size array.",index, res.len());
                    }
                    res[index] += 1;
                }
            }
        }
        println!("{}/{}...", a, 255);
    }
    if nbcomputations != 256*256*256*256 {
        panic!("nbcomputations different than expected");
    }
    return res;
}

pub fn midorimixcolumns(input:&ByteVec) -> ByteVec {
    let m = [
        [0x0,0x1,0x1,0x1],
	    [0x1,0x0,0x1,0x1],
	    [0x1,0x1,0x0,0x1],
	    [0x1,0x1,0x1,0x0]
    ];
    let mut res = vec![];
    for i in 0..4 {
        let m0 = &m[i][0];
        let m1 = &m[i][1];
        let m2 = &m[i][2];
        let m3 = &m[i][3];
        let s0 = &input.a;
        let s1 = &input.b;
        let s2 = &input.c;
        let s3 = &input.d;
        res.push( (m0*s0) ^ (m1*s1) ^ (m2*s2) ^ (m3*s3) );
    }
    return ByteVec::new(res[0],res[1],res[2],res[3]);
}

pub fn aesmixcolumns(input:&ByteVec) -> ByteVec {
    let r:Vec<u8> = vec![input.a, input.b, input.c, input.d];
    let mut a:Vec<u8> = vec![0;4];
    let mut b:Vec<u8> = vec![0;4];
    for c in 0..4 {
        a[c] = r[c];
        let h2:i8 = r[c] as i8;
        let h3:i8 = h2 >> 7;
        let h:u8 = h3 as u8;
        b[c] = r[c] << 1;
        b[c] ^= 0x1B & h;
    }
    return ByteVec::new(
        b[0] ^ a[3] ^ a[2] ^ b[1] ^ a[1], /* 2 * a0 + a3 + a2 + 3 * a1 */
        b[1] ^ a[0] ^ a[3] ^ b[2] ^ a[2], /* 2 * a1 + a0 + a3 + 3 * a2 */
        b[2] ^ a[1] ^ a[0] ^ b[3] ^ a[3], /* 2 * a2 + a1 + a0 + 3 * a3 */
        b[3] ^ a[2] ^ a[1] ^ b[0] ^ a[0], /* 2 * a3 + a2 + a1 + 3 * a0 */
    );
}


fn main() {
    // let res = generate_clauses(vec![
    //     clause_from_vec(vec![0,1,2]),
    //     clause_from_vec(vec![2,3,4]),
    // ], 4);
    let inputclauses:Vec<XorClause> = (vec![
        vec![1,2,3],
        vec![3,31,5],
        vec![5,32,7],
        vec![7,33,9],
        vec![9,34,1],
        vec![19,34,11],
        vec![17,36,19],
        vec![15,32,17],
        vec![13,35,15],
        vec![11,12,13],
        vec![21,22,23],
        vec![23,35,25],
        vec![25,33,27],
        vec![27,36,29],
        vec![29,31,21],
    ]).iter().map(|e| clause_from_vec(e.to_vec())).collect();
    let res_4 = generate_clauses(&inputclauses, 4);
    println!("res_4:\n{:?}",res_4);
    println!("restest\n{:?}", generate_clauses(&inputclauses,3));
    let res_more = generate_clauses(&inputclauses, 6);
    // println!("\nres_more:\n{:?}",res_more);
    let mut filtered_res_more:Vec<XorClause> = vec![];
    for e in res_more {
        // if e.contains(2) && e.contains(12) && e.contains(22) {
        //     println!("{:?}",e);
        // }
        if e.len() <= 4 {
            filtered_res_more.push(e);
        }
    }
    println!("\nfiltered_res_more:\n{:?}",filtered_res_more);
    println!("size4: {}\t sizen: {}", res_4.len(), filtered_res_more.len());

    // println!("### start compute mds");
    // // test vectors
    // // println!("test vector: {:?}", aesmixcolumns(&ByteVec::new(219,19,83,69)));
    // println!("test vector: {:?}", aesmixcolumns(&ByteVec::new(242,10,34,92)));
    // // println!("test vector: {:?}", aesmixcolumns(&ByteVec::new(1,1,1,1)));
    // // run tests
    // let res = computemds(aesmixcolumns);
    // // display result table
    // println!("result occurences");
    // for (i,&e) in res.iter().enumerate() {
    //     if e > 0 {
    //         println!("{:08b}:\t{}",i as u8,e);
    //     }
    // }
    // // count nb occurences
    // let mut nbuniquevalues:usize = 0;
    // let mut nbtotal:usize = 0;
    // for &e in res.iter() {
    //     nbtotal += e;
    //     if e > 0 {
    //         nbuniquevalues += 1;
    //     }
    // }
    // if nbtotal != 256*256*256*256 {
    //     panic!("error in total elements ({})", nbtotal);
    // }
    // println!("nb unique values: {}", nbuniquevalues);
}
