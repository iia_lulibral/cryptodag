use std::collections::{HashSet};
use bit_set::BitSet;

pub type Variable = usize;
pub type XorClause = BitSet;

pub fn clause_from_vec(v:Vec<usize>) -> XorClause {
    let mut res = XorClause::new();
    for e in v {
        res.insert(e);
    }
    return res;
}

pub fn generate_clauses(clauses:&Vec<XorClause>, maxsize:usize) -> HashSet<XorClause> {
    let mut res:HashSet<XorClause> = HashSet::new();
    let mut new_clauses = clauses.clone();
    while !new_clauses.is_empty() {
        // add new_clauses to res
        while !new_clauses.is_empty() {
            res.insert(new_clauses.pop().unwrap());
        }
        // iterate over all couples of active clauses
        for c1 in res.iter() {
            for c2 in res.iter() {
                let mut inter = c1.clone();
                inter.intersect_with(c2);
                if !inter.is_empty() {
                    let mut union = c1.clone();
                    union.union_with(c2);
                    union.symmetric_difference_with(&inter);
                    if union.len() <= maxsize && !res.contains(&union) {
                        new_clauses.push(union);
                    }
                }
            }
        }
    }
    return res;
}