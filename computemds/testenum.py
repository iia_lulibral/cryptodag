def testenum():
    full_registered = []
    for a1, a2 in cartesian_product(0..1, 0..1):
        dom_1 = 0..0
        if a1:
            dom_1 = 1..256
        dom_2 = 0..0
        if a2:
            dom_2 = 1..256
        registered = set()
        for c1,c2 in cartesian_product(dom_1,dom_2):
            compute_abstraction_and_register_results(c1,c2) # ex: compute_abstraction_and_register_results(3,2) -> [1,1,1]
            if len(registered) == 2^nb_output_bytes:
                break # no need to go further
        full_registered.append(registered)
    return registered

"""
a1=0, a2=0 (|prod|=1)    1
a1=1, a2=0 (|prod|=255)  2
a1=0, a2=1 (|prod|=255)  2 
a1=1, a2=1 (|prod|=255*255=65536)  2

1 + 255x2 + 2

1 1 -> 0
1 2 -> 1


"""