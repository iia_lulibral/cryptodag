# CryptoDag

Defines cryptosystems as Directed Acyclic Graphs and builds automatically differential attacks.

## Installation

run bundler to install requirements:

```
bundle install
```

## Architecture

 - *cryptosystems/*: implements various cryptosystems (midori64, AES, *etc.*)
 - *writers/*: implements optimization frameworks output. For instance, a minizinc writer that generates attacks from a DAG
 - *cryptonodes/*: implments various nodes (Xor, ShuffleCell, *etc.*)
 - *tests/*: unit and integration tests for the components

# commands

## unit testing


## Cipher automatic grading

1. Uses all its inputs? (Yes/No)


## graphviz

executes main executable and get the dot file

```sh
./main.rb > tmp/test.dot
```

runs graphviz (`sudo apt-get intall graphviz`) and generate the *.png* file.

```sh
dot -T png tmp/test.dot -o test.png
``` 

# todo

- [X] DAG datastructure
- [X] simple graphviz implementation
- [X] Input/Output node
- [X] ixColumns node
- [X] shuffleCell node
- [X] subCell node
- [X] Xor node
- [X] Midori64 base implementation (encrypts)
- [X] Step1opt midori64
- [X] midori128 implementation
- [ ] DAGviz : add operator type (XorNode, etc.)
- [ ] AES implementation
- [ ] SKINNY implementation
- [ ] spark implementation
- [ ] DAGviz : hypergraph representation 



# TODO (refactor)

- [ ] InputNode with constant (use DTable constant set-up instead of inserting directly into each element: nodes/input.rb)