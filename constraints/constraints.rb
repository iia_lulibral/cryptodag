require_relative "model"
require_relative "expression"

# Abstract class Constraint
# Constraint in the constraint solver
class Constraint
  include Visited
end

## Binary constraint - Functionnal form : a = b
class Binary < Constraint
  attr_reader :v1, :v2

  ## Ctor
  # @param v1 [ expression or number ] first operand
  # @param v2 [ expression or number ] second operand
  def initialize(v1, v2)
    super()
    @v1 = v1
    @v2 = v2
    @v1.above << self
    @v2.above << self
  end

  def accept(visitor)
    visitor.visit(self)
    @v1.accept(visitor)
    @v2.accept(visitor)
  end

  def substitute(from, to)
    if @v1 == from
      @v1 = to
      @v1.above << self
    end

    if @v2 == from
      @v2 = to
      @v2.above << self
    end
  end
end

# Equality constraint - Functionnal form : a = b
class Equality < Binary
  def to_s
    "#{v1} == #{v2}"
  end
end

## Binary constraint - Functionnal form : a = b
class Different < Binary
  def to_s
    "#{v1} != #{v2}"
  end
end

# Equivalence:   a <=> b
#
# @param parameters [ list of parameters ] parameters to XORed
class Equivalence < Binary
  def to_s
    "#{v1} <=> #{v2}"
  end
end

## Diff1(variables) => Sum(variables) != 1
class Diff1 < Different
  attr_reader :parameters

  ## Ctor
  # @param parameters [ Expressions ] The expressions to sum
  def initialize(*parameters)
    super(Sum.new(*parameters), LitteralValue.new(1))
  end
end

## An expression belongs to a set
class IncludedIn < Constraint
  attr_reader :expression, :set
  ## Ctor
  #
  # @param expression [ Expression ] the expression whose result is in *set*
  # @param set [ SetValue or Array of values ]
  def initialize(expression, set)
    @expression = expression

    set = SetValue.new(set) unless set.kind_of?(SetValue)
    @set = set
  end

  def to_s
    "#{v1} In {#{v2}}"
  end
end


## table constraint (the assignment of variables belong into a set of variables)
class TableConstraint < Constraint
  attr_reader :expression, :table
  
  def initialize(expression, table)
    @expression = expression
    @table = table
  end
end