require "minitest/autorun"
require "pry"

require_relative "../variables"

class TestModel < MiniTest::Unit::TestCase
  def setup
  end

  def test_initialize
    variables = [
      Variable.new("douze", "int", 12),
      Variable.new("myInt", "int"),
      Variable.new("2..15", 2..15),
    ]
    variables.each do |v|
      assert_equal(SimpleVariableDeclaration, v.declaration)
    end
  end

  def test_new_variable_matrix
    matrix = Variable.new_variable_matrix("chene", 0..15, [4, 4, 12])
    assert_instance_of(Variable, matrix[3, 3, 11])
    assert_equal("chene_3_3_11", matrix[3, 3, 11].name.to_s)
  end
end
