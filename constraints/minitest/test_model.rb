require "minitest/autorun"
require "pry"

require_relative "../all"

class TestModel < MiniTest::Unit::TestCase
  def setup()
    @empty = Model.new
    @model = Model.new
    v = [Variable.new("a", "int"), Variable.new("b", "int"), Constant.new("z", 2), Variable.new("c", "int")]

    constraint = Equality.new(
      Add.new(
        v[0],
        Add.new(
          v[1],
          v[2]
        )
      ),
      v[3]
    )
    @model.add_variables(*v)
    @model.add_constraints(constraint)
  end

  def test_model_declares_variables
    @model.build_declarations
    assert_equal(4, @model.declarations.reject { |d| d.class == NoDeclaration }.count)
  end

  def test_constraint_visitor
    variables = Hash.new(0)
    visitor = Visitor.new { |e| variables[e] += 1 if e.class <= Variable }
    @model.accept_constraints(visitor)
    assert_equal(variables.count, 3)
    assert(variables.values.all? { |count| count == 1 })
  end

  def test_VariablesTable_should_declare_many_variables
    v = VariablesTable.new("ShouldNotBeDeclared", "int", [4, 4])
    @empty.add_variables(v, *v.variables)
    @empty.build_declarations

    assert_equal(v.count, @empty.declarations.reject { |d| d.class == NoDeclaration }.count)
  end

  def test_Table_should_declare_one_variable_only
    v = Table.new("ShouldBeDeclared", "int", [4, 4])
    @empty.add_variables(v)
    @empty.build_declarations

    assert_equal(1, @empty.declarations.reject { |d| d.class == NoDeclaration }.count)
  end

  def test_ConstantTable_should_declare_one_variable_only
    v = ConstantTable.new("ShouldBeDeclared", "int", [4, 4])
    @empty.add_variables(v)
    @empty.build_declarations

    assert_equal(1, @empty.declarations.reject { |d| d.class == NoDeclaration }.count)
  end
end
