class Expression
  attr_accessor :above

  def initialize
    @above = []
  end
end

# Functionnal form : f(a,b)
class Function < Expression
  include Visited
  attr_reader :name, :parameters

  ## Ctor
  # @param name [ String ] name of the function
  # @param parameters [ list of parameters ] parameters of the function
  def initialize(name, *parameters)
    super()
    @name = name
    @parameters = parameters.map do |p|
      basic = [Integer, Float]
      if basic.include?(p.class)
        LitteralValue.new(p)
      else
        p
      end
    end
    @parameters.each { |e|
      e.above.push(self)
    }
  end

  def accept(visitor)
    visitor.visit(self)
    accept_elements(visitor, @parameters)
  end

  def substitute(from, to)
    @parameters.map! do |param|
      if param == from
        to.above << self
        to
      else
        param
      end
    end
  end

  def be_replaced_by(expr)
    above.each { |e| e.substitute(self, expr) }
    above = []
  end

  def to_s
    "#{@name}( #{@parameters.join(" , ")} )"
  end
end

# Abstract class Operation. An operation has an infix operator
class Operation < Function
end

# Priority form : (a,b)
#
# @param parameters [ list of parameters ] parameters of the priority
class Priority < Function
  include Visited

  def initialize(parameter)
    super("()", [parameter])
  end
end

# An operator in the constraint solver
#
# @param operator [ String ] an operator
class Operator < Expression
  include Visited
  attr_reader :operator

  def initialize(operator)
    super()
    @operator = operator
  end

  def to_s
    @operator.to_s
  end
end

# Operation XOR
#
# @param parameters [ list of parameters ] parameters to XORed
class Xor < Operation
  def initialize(*parameters)
    super(Operator.new("xor"), *parameters)
  end
end

# Operation Modulo ^
#
# @param parameters [ list of parameters ] modulo realized between these parameters
class Modulo < Operation
  def initialize(*parameters)
    super(Operator.new("mod"), *parameters)
  end
end

# Operation And
#
# @param parameters [ list of parameters ] parameters to Anded
class And < Operation
  def initialize(*parameters)
    super(Operator.new("and"), *parameters)
  end
end

# Operation Multiplication
#
# @param parameters [ list of parameters ] parameters to multiplied
class Mult < Operation
  def initialize(*parameters)
    super(Operator.new("*"), *parameters)
  end
end

# Operation Division
#
# @param parameters [ list of parameters ] parameters to divided
class Div < Operation
  def initialize(*parameters)
    super(Operator.new("/"), *parameters)
  end
end

## Add two elements (@see also Sum)
#
# @param parameters [ list of parameters ] parameters to Added
class Sum < Operation
  def initialize(*parameters)
    super(Operator.new("+"), *parameters)
  end
end

## Substraction
#
# @param parameters [ list of parameters ] parameters to Subtracted
class Sub < Operation
  def initialize(*parameters)
    super(Operator.new("-"), *parameters)
  end
end

## Equals
#
# @param parameters [ list of parameters ] parameters to be compared
class Equals < Operation
  def initialize(*parameters)
    super(Operator.new("="), *parameters)
  end
end

<<<<<<< HEAD
class EqualsExpr < Priority
  def initialize(a,b)
    super(Equals.new(a,b))
  end
end

class DiffOp < Operation
  def initialize(*parameters)
    super(Operator.new("!="), *parameters)
  end
end

class DiffExpr < Priority
  def initialize(a,b)
    super(DiffOp.new(a,b))
  end
end

## TableSum
#
# @param table table to be summed
# @param indices names for the indices in the sum
class TableSum < Operation
  attr_accessor :table
  attr_accessor :indices
  def initialize(table, indices)
    @table = table
    @indices = indices
=======
## A litteral value (int, number...)
class LitteralValue < Expression
  include Visited
  attr_reader :value

  ## Ctor
  #
  # @param value [ Any numeric value ] Any value
  def initialize(value)
    @value = value
    super()
  end

  def accept(visitor)
    visitor.visit(self)
  end

  def to_s
    "#{value}"
>>>>>>> 817502dfdf83b9a9a56636ba65630f3793319a2e
  end
end
