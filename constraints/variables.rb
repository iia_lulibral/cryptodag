require "forwardable"

require_relative "model"
require_relative "types"
require_relative "declarations"
require_relative "expression"
require_relative "../tools/multimatrix"

## A variable in the constraint solver
#
# @param name [ String ] name of the variable
# @param type [ int,bool,float,CSet or String ] type of the variable
# @param value [ value defined ] value of the variable initialized by default at nil
class Variable < Expression
  include Visited
  attr_reader :name, :type, :value

  def initialize(name, type, value = nil)
    super()
    name = Name.new(name) if name.class == String

    case type
    when String
      type = Type.new(type)
    when CSet
      type = type.name
    when Range
      type = Interval.new(type.first, type.last)
    end

    @name = name
    @type = type
    @value = value
  end

  def declaration
    SimpleVariableDeclaration
  end

  def use(variable)
    variable == self ? 1 : 0
  end

  ## Does nothing!
  def substitute(from, to)
  end

  def be_replaced_by(expr)
    above.each { |e| e.substitute(self, expr) }
    above = []
  end

  ## Create a Multimatrix full of variables
  #
  # @param name [ String ] : Base name for the variables
  # @param type [ Type ]   : Type of the variables
  # @param dimensions [ Array(int) ]  : dimensions of the Matrix
  # @param domain [ Interval ] : Domain of the variable
  #
  # @return a matrix of variables
  def self.new_variable_matrix(name, type, dimensions, domain = nil)
    matrix = MultiMatrix.build("var_#{type}", dimensions)

    matrix.each_index do |*coordinates|
      extension = coordinates.join("_")
      matrix[*coordinates] = Variable.new("#{name}_#{extension}", type, domain)
    end

    matrix
  end

  private

  ## Create a new loop variable with an unique name
  #
  # @param name [ String ] Base name of the variable
  # @param type [ Type ] type of the variable
  # @return a new variable with a name of the form "name_index" where index is serial integer so that the name is unique
  def self.new_loop_variable(name, type, value = nil)
    raise "Should not be called! Will be deleted!"
    #name = SingletonVariableStore.instance.new_loop_variable(name)
    return Variable.new(name, type, value)
  end

  ## Create a new unique variable if the name didn't exist in the store of variables
  #
  # @param name [ String ] Name of the variable
  # @param type [ Type ] type of the variable
  # @return a new variable if the name is not contain in the store of variables
  def self.new_unique(name, type, value = nil)
    raise "Should not be called! Will be deleted!"
    name = SingletonVariableStore.instance.new_unique_variable(name)
    return Variable.new(name, type, value)
  end

  public

  def self.new_array(name, type, dimensions, value = nil)
    raise "Should not be called. Use"
    matrix = MultiMatrix.build(type, dimensions)
    matrix.each_with_index do |v, *dim|
      name_dim = name + "_" + dim.join("_")
      matrix[*dim] = Variable.new(name_dim, type)
    end
    return matrix
  end

  def to_s
    @name.to_s
  end
end

## A name of an element in the constraint solver
#
# @param name [ String ] name
class Name
  include Visited
  attr_reader :name

  def initialize(name)
    @name = name
  end

  # return a String corresponding to the name
  def to_s
    @name
  end
end

# Abstract class CSet
# A set in the constraint solver
class CSet < Variable
  def declaration
    return CSetDeclaration
  end
end
