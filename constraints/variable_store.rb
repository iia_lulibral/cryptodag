require "set"
require "singleton"

## Store for all the variables of the CSP. Singleton: use VariableStore.instance
#
# @param loop_variables  [ Hash ] contains all loop variables
# @param unique_variables [ Set ] contains all unique variables
class VariableStore
  include Singleton
  include Visited

  def initialize
    @loop_variables = Hash.new(0)
    @unique_variables = Set.new
  end

  # Generate a new variable name for a loop
  #
  # @param varName [ String ] Base name for the variable
  # @return a variable name of the form "varName_index" where index is serial integer so that the name is unique
  def new_loop_variable(varName)
    name = "#{varName}_#{@loop_variables[varName]}"
    @loop_variables[varName] += 1
    name
  end

  # Determine if a name is contained in the Hash that contains all loop variables
  #
  # @param name [ String ] Name to search in the Hash
  # @return Boolean : true if name exists else false
  private def unique(name)
    return false if @loop_variables.has_key?(name)
    return false if @unique_variables.include?(name)
    true
  end

  # Registers a variable name.
  #
  # @raise String if name is already used
  def new_unique_variable(name)
    throw "Variable #{name} existe déjà (#{@unique_variables})" unless unique(name)
    @unique_variables << name
    name
  end
end

class SingletonVariableStore < VariableStore
  include Singleton
end
