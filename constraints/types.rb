require_relative "declarations"
require_relative "variable_store"

# A named constant in the constraint solver
#
# @param name [ String ] name of the constant
# @param value [ constant value ] value of the constant
# @param type [ int,bool,float,CSet or String ] type of the constant initialized by default at 'int'
class Constant
  include Visited
  attr_reader :name, :value, :type

  def initialize(name, value, type = "int")
    # @name = SingletonVariableStore.instance.new_unique_variable(name)
    @name = name
    @value = value
    @type = Type.new(type)
  end

  def declaration
    return ConstantDeclaration
  end
end

# Abstract class TypeOrInterval
class TypeOrInterval
  include Visited
end

# A type in the constraint solver
#
# @param type [ int,bool,float ] type of an element (variable,constant,...)
# @raise String if type didn't exist
class Type < TypeOrInterval
  attr_reader :name

  @@allowed = %w{ int bool float }

  def initialize(name)
    raise "Type should be amongst #{@@allowed} (Got #{name.inspect})" unless @@allowed.include?(name)
    @name = name
  end
end

# An interval in the constraint solver
#
# @param min [ int ] the beginning of the interval
# @param max [ int ] the end of the interval
class Interval < TypeOrInterval
  attr_reader :min, :max

  def initialize(min, max)
    @min = min
    @max = max
  end

  def size_in_bits
    case @max
    when 15
      4
    when 255
      8
    when 65535
      16
    else
      raise 'Why do you need to find the size in bits of #{@min..@max} ?'
    end
  end
end

## A litteral set (no value associated)
class SetValue
  include Visited

  attr_reader :values

  ## Ctor
  #
  # @param values The values in the set
  def initialize(values)
    @values = values
  end
end
