require "set"
require "singleton"

require_relative "../tools/visitor"

# Model contains all the variables, constraints, instructions. Singleton: use Model.instance
#
# @param constraints [ ruby's table ] contains all constraints of model
# @param variables [ ruby's table ] contains all variables of model
# @param instructions [ ruby's table ] contains all instructions of model
# @param constants [ ruby's table ] contains all constants of model
# @param set [ Set ] set of variables
class Model
  include Visited

  attr_reader :constraints, :variables, :declarations, :instructions, :to_display

  def initialize
    @declarations = []
    @constraints = []
    @variables = []
    @instructions = []
    @constants = []
    @set = Set.new
    @to_display = []
  end

  ## How many times is a variable used ?
  #
  # @param variable [ Variable ] the variable to search for in constraints
  # @return [ int ] the number of occurences of the variable in all the constraints
  def uses(variable)
    uses = 0
    visitor = ModelVisitor.new do |element|
      $stderr.puts element.class
      if self.class <= Variable
        uses += 1 if element == self
      end
    end
    accept(visitor)
    uses
  end

  # Add constraints to model
  #
  # @param constraints [ String ] represents one or more constraints
  def add_constraints(*constraints)
    @constraints += constraints
  end

  ## Add a comment
  #
  # @param comment [ String or Comment ] Comment to be added
  def add_comment(comment)
    comment = Comment.new(comment) unless comment.kind_of?(Comment)
    @constraints << comment
  end

  # Add constants to model
  #
  # @param constants [ String ] represents one or more constants
  def add_constants(*constants)
    @constants += constants
  end

  # Add variables to model
  #
  # @param variables [ String ] represents one or more variables
  def add_variables(*variables)
    @variables += variables
  end

  # Add instructions to model
  #
  # @param instructions [ String ] represents one or more instructions
  def add_instructions(*instructions)
    @instructions += instructions
  end

  # Add variables to display by the solver
  #
  # @param text [ String ] text describing what is displayed
  # @param variables [ Expanded list of variables ] The variables to display
  def add_display(text, *variables)
    @to_display << Display_Cipher.new(text, *variables)
  end

  def add_display_matrix(text, name, matrix)
    @to_display << Display_Array2D.new(text, name, matrix)
  end

  def accept_constraints(visitor)
    accept_elements(visitor, @constraints)
  end

  ## Prepare declarations for all the variables
  def build_declarations
    @declarations += declare(@variables)
    @declarations += declare(@constants)
  end

  private def declare(variables)
    variables.map do |var|
      var.declaration.new(var)
    end
  end
end

class SingletonModel < Model
  include Singleton
end

# Instruction not defining a constraint. Very specific to the solver
#
# @param str [ String ] corresponds at an instruction
class Instruction
  include Visited
  attr_reader :str

  def initialize(str)
    @str = str
  end
end

# A Display is a list of variables to display
#
class Display
  include Visited
  attr_reader :text

  def initialize(text)
    @text = text
  end
end

class Display_Array2D < Display
  attr_reader :name, :matrix

  def initialize(text, name, matrix)
    super(text)
    @name = name
    @matrix = matrix
  end
end

class Display_Cipher < Display
  attr_reader :variables

  def initialize(text, *variables)
    super(text)
    @variables = variables
  end
end
