## Comments for a Solver model
class Comment
  attr_reader :value

  ## Ctor
  # @param comment [ String ] The new comment
  def initialize(comment)
    @value = "#{comment}#{info_from_caller}"
  end

  private def info_from_caller
    if caller.size >= 4
      " (#{caller[3].split(":")[0..1].join(":")})"
    else
      # Comment was added in a strange way :
      # Should be Comment#info_from_caller < Comment#initiatize < Model#add_comment < HERE
      ""
    end
  end

  def to_s
    " #{@value}"
  end
end
