## Base for variable *declaration*. Only type is important
class AbstractVariableDeclaration
  include Visited
  attr_reader :variable

  def initialize(variable)
    @variable = variable
  end
end

class SimpleVariableDeclaration < AbstractVariableDeclaration
end

class ConstantDeclaration < AbstractVariableDeclaration
end

class CSetDeclaration < AbstractVariableDeclaration
end
