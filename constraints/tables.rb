require_relative "variables"

require_relative "../tools/multimatrix.rb"

class AbstractTable < Variable
  extend Forwardable

  def_delegators :@matrix, :dimensions, :each, :each_index, :each_with_index, :[], :to_a, :count, :set, :index, :coordinates

  ## Initialize
  # @param name [ String ] Name of the Table
  # @param type [ Type ]   : Type of the variables
  # @param dimensions [ Array(int) ]  : dimensions of the Matrix
  # @param domain [ Array ] : values of the variable
  def initialize(name, type, dimensions, value = nil)
    super(name, type, value)

    @matrix = MultiMatrix.build(type, dimensions)
    @matrix.each_index do |*coordinates|
      @matrix[*coordinates] = build_one_element(coordinates)
    end
  end

  protected def build_one_element(coordinates)
    raise "Should be redefined"
  end
end

## Table which will remain in the solver (one array variable)
class Table < AbstractTable
  protected def build_one_element(coordinates)
    VariableInTable.new(self, coordinates)
  end

  def declaration
    TableDeclaration
  end
end

## Table which will remain in the solver (one array variable), with constant values
class ConstantTable < Table
  protected def build_one_element(coordinates)
    VariableInTable.new(self, coordinates)
  end

  def declaration
    ConstantTableDeclaration
  end
end

## Table containing many independant variables
class VariablesTable < AbstractTable
  attr_reader :variables

  def initialize(*args)
    @variables = []
    super(*args)
  end

  protected def build_one_element(coordinates)
    extension = coordinates.join("_")
    v = Variable.new("#{name}_#{extension}", type, value)
    @variables << v
    v
  end

  def declaration
    NoDeclaration
  end
end

class TableDeclaration < AbstractVariableDeclaration
end

class ConstantTableDeclaration < AbstractVariableDeclaration
end

## Table access by multiple dimension index
class VariableInTable < Variable
  include Visited
  attr_reader :table, :indexes

  ## Initialize a table access by index
  # @param table [ Table ] Table containing the variable
  # @param index [ array ] Array of indexes (as many as the table dimension). May not be integer (variables...)
  def initialize(table, indexes)
    @table = table
    @indexes = indexes
    super(name, table.type)
  end

  ## VariableInTable has no declaration
  def declaration
    NoDeclaration
  end

  def name
    "#{@table.name}[#{@indexes.join(",")}]"
  end
end

class NoDeclaration < AbstractVariableDeclaration
end

## Table of variable with dynamic Instanciation choice
class DTable < AbstractTable
  attr_reader :declarationMode, :constant, :variables

  ## Ctor
  def initialize(*params, constant: false)
    @constant = constant

    @variables = []
    @declarationMode = :unfolded
    super(*params)

    if @constant && value == nil
      raise "Can't build a Constant Table without value"
    end
  end

  ## Change declaration Mode
  # * Variable tables can be :unfolded or :folded
  # * Constraint tables can be :folded or :unfolded or :values
  # @param mode [ symbol ] Set the mode to be :unfolded, :folded or :value
  def declarationMode=(mode)
    raise "Variable table can't be declared by values" if mode == @value && !@constant
    @declarationMode = mode
  end

  def build_one_element(coordinates)
    elt_name = "#{name}_#{coordinates.join("_")}"
    if value
      val = value[index(*coordinates)]
    else
      val = nil
    end
    v = DynamicVariable.new(self, coordinates,
                            elt_name, type, val)
    @variables << v
    v
  end

  def declaration()
    case @declarationMode
    when :folded
      if @constant
        ConstantTableDeclaration
      else
        TableDeclaration
      end
    when :unfolded, :value
      NoDeclaration
    end
  end
end

# A table variable whose instanciation type is dynamic
class DynamicVariable < Variable
  attr_reader :indexes, :table
  attr_accessor :value # TODO(all) remove this after refactoring of DTable

  def initialize(table, indexes, *params)
    @table = table
    @indexes = indexes
    super(*params)
  end

  def declarationMode()
    @table.declarationMode
  end

  def declaration()
    case @table.declarationMode
    when :folded, :value
      NoDeclaration
    when :unfolded
      SimpleVariableDeclaration
    end
  end
end
