require "pry"

class MultiMatrix
  include Enumerable
  attr_reader :type, :dimensions

  private def initialize(type, dimensions, data)
    @type = type
    @dimensions = dimensions.dup
    @dimensions.freeze # If they change, access control is dead
    @data = data
  end

  ## Replace each nil entry in the table by a vector
  def self.add_level(array, number)
    array.each_with_index do |v, index|
      if v == nil
        array[index] = Array.new(number, nil)
      else
        add_level(v, number)
      end
    end
  end

  def self.build(type, dimensions)
    tmp = [nil]
    dimensions.each do |dimension|
      MultiMatrix.add_level(tmp, dimension)
    end
    return MultiMatrix.new(type, dimensions, tmp[0])
  end

  def self.clone(type, coordinates, data)
    return self.new(type, coordinates, data)
  end

  private def check(coordinate, dimension)
    if dimension >= @dimensions.size
      raise DimensionError.new("Matrix as not so many dimensions ( #{dimension} >= #{@dimensions.size} ) ")
    end
    # binding.pry
    return if coordinate < @dimensions[dimension]
    raise DimensionError.new("Index error on matrix for the dimension #{dimension} ( #{coordinate} > #{@dimensions[dimension]} ) ")
  end

  private def check_all_dimensions(coordinates)
    coordinates.each_with_index { |coord, dim| check(coord, dim) }
  end

  ## Access the value at a give coordinate
  #
  # @param coordinates [ Array(int) ] The coordinate, have to belong to the matrix
  # @return The corresponding value, either a single value or a submatrix
  def [](*coordinates)
    tmp = @data

    coordinates.each_with_index do |c, dimension|
      check(c, dimension)
      tmp = tmp[c]
    end
    if coordinates.size < @dimensions.size
      remaining = @dimensions.last(@dimensions.size - coordinates.size)
      tmp = MultiMatrix.clone(@type, remaining, tmp)
    end

    tmp
  end

  ## Set a value at a given coordinate
  #
  # @param value The value to set in the matrix
  # @param coordinates [ Array(int) ] The coordinate, have to belong to the matrix
  # @return The corresponding value, either a single value or a submatrix
  def []=(*coordinates, value)
    last = coordinates.pop
    tmp = @data

    coordinates.each_with_index do |c, dimension|
      check(c, dimension)
      tmp = tmp[c]
    end

    check(last, coordinates.size)
    tmp[last] = value
  end

  protected def inc_higher_dimension_first(coordinates)
    dimension = 0
    @dimensions.size.times do |dimension|
      if coordinates[dimension] + 1 == @dimensions[dimension]
        coordinates[dimension] = 0
      else
        coordinates[dimension] += 1
        return coordinates
      end
    end
    return nil
  end

  protected def inc_lower_dimension_first(coordinates)
    dimension = 0
    last_dimension = @dimensions.size - 1

    last_dimension.downto(0) do |dimension|
      if coordinates[dimension] + 1 == @dimensions[dimension]
        coordinates[dimension] = 0
      else
        coordinates[dimension] += 1
        return coordinates
      end
    end
    return nil
  end

  alias :inc :inc_lower_dimension_first

  ## run a code on every element of the matrix
  #
  # @param code [ Proc ] the code to run
  # @return the matrix itself
  def each(&code)
    coordinates = Array.new(@dimensions.size, 0)
    while coordinates
      code.yield(self[*coordinates])
      coordinates = inc(coordinates)
    end
    self
  end

  ## run a code on every coordinates of the matrix
  #
  # @return the matrix itself
  def each_index(&code)
    coordinates = Array.new(@dimensions.size, 0)
    while coordinates
      code.yield(*coordinates)
      coordinates = inc(coordinates)
    end
    self
  end

  ## Convert a linear position starting by 0 to matrix coordinates
  #
  # @param index [ Int ] The position of the element, same order as each
  # @return An array with the coordinates of the index'th elemet
  def coordinates(index)
    raise DimensionError if index >= count

    dimensions = @dimensions.dup
    dimensions.shift

    coordinates = Array.new(@dimensions.size)

    dim = 0
    while dimensions.size != 0
      length = dimensions.inject(1) { |a, b| a * b }
      coordinates[dim] = index / length
      index -= length * coordinates[dim]

      dimensions.shift
      dim += 1
    end
    coordinates[dim] = index
    #    [ a,b,c,d ]
    #    a*dimensions[1]*dimensions[2]*dimensions[3] +
    #    b*dimensions[2]*dimensions[3] +
    #    c*dimensions[3] +
    #    d

    coordinates
  end

  ## Converts coordinates to linear position starting by 0
  #
  # @param coordinates [ Expanded aarray of int ] An array with the coordinates of the index'th element
  # @return The position of the element, same order as each, starting with 0

  def index(*coordinates)
    check_all_dimensions(coordinates)

    dimensions = @dimensions.dup
    dimensions.shift

    index = 0
    coordinates.each do |coord|
      length = dimensions.inject(1) { |a, b| a * b }
      index += coord * length
      dimensions.shift
    end

    return index
  end

  ## Return an element by linear position starting by 0
  #
  # @param index [ int ] Position of the element in the array, starting by 0
  # @return The element at position index
  def at(index)
    self[*coordinates(index)]
  end

  ## Set an element referenced by linear position
  #
  # @param index [ int ] Position of the element in the array, starting by 0
  # @param value The new value of the element
  # @return The element at position index
  def set_at(index, value)
    self[*coordinates(index)] = value
  end

  ## run a code on every element of the matrix, adding the index
  #
  # @return the matrix itself
  def each_with_index(&code)
    coordinates = Array.new(@dimensions.size, 0)
    while coordinates
      code.yield(self[*coordinates], *coordinates)
      coordinates = inc(coordinates)
    end
    self
  end

  ## Return the number of values of the matrix
  def count
    @dimensions.inject(1) { |a, b| a * b }
  end

  ## Return an array of all the elements
  def to_a
    values = []
    each do |value|
      values << value
    end

    values
  end

  ## Set a matrix from an array.
  #
  # @param array [ Array ] Array of values. The array order must be the same as the one got with a each.
  def set(array)
    raise DimensionError if array.size != count

    cpt = 0
    self.each_index do |*coord|
      self[*coord] = array[cpt]
      cpt += 1
    end

    raise DimensionError if cpt != array.size
  end

  ## Fills the matrix with a value
  #
  # @param value any value
  def fill(value)
    each_index { |*dim| self[*dim] = value }
  end

  ## Return the matrix as a set of arrays
  def arrays
    @data.clone.freeze
  end

  def sub_matrix(*sub_range)
    cdr = sub_range.map{ |r| r.to_a }
    car = cdr.shift

    shift = sub_range.map(&:first)
    size = sub_range.map(&:size)
    destination = Array.new(sub_range.size)

    matrix = MultiMatrix.build(@type, size)

    car.product(*cdr) do |coordinates|
      check_all_dimensions(coordinates)

      coordinates.each_index do |i|
        destination[i] = coordinates[i] - shift[i]
      end
      matrix[*destination] = self[*coordinates]
    end
    matrix
  end


  class DimensionError < Exception
  end
end
