require "minitest/autorun"
require_relative "../multimatrix.rb"
require "pry"

class TestMultiMatrix < MiniTest::Unit::TestCase
  def setup
    @m = MultiMatrix.build(:int, [3, 4, 5])
    @m1 = MultiMatrix.build(:int, [15])
    @m2 = MultiMatrix.build(:int, [3, 4])
    @m4 = MultiMatrix.build(:int, [4, 4, 4, 15])
    @matrices = [@m, @m1, @m2, @m4]
  end

  def test_to_a
    m = MultiMatrix.build(:int, [2, 2])
    m.set([1, 3, 2, 4])
    assert_equal([1, 3, 2, 4], m.to_a)
  end

  def test_attributes_are_stored
    assert_equal(3, @m.dimensions[0])
    assert_equal(4, @m.dimensions[1])
    assert_equal(5, @m.dimensions[2])
    assert_equal(:int, @m.type)
  end

  def test_can_store_an_integer_and_access_it
    @m[1, 2, 3] = 1848
    assert_equal(1848, @m[1, 2, 3])

    @m[1][2][2] = 1789
    assert_equal(1789, @m[1, 2, 2])
    assert_equal(1789, @m[1][2][2])
  end

  def test_can_access_a_submatrix_as_a_vector
    d = @m[1, 2]
    assert_equal(1, d.dimensions.size)
    assert_equal(5, d.dimensions[0])
    d[4] = 1917
    assert_equal(1917, d[4])
    assert_equal(1917, @m[1, 2, 4])
  end

  def test_can_access_a_submatrix_as_a_matrix
    @m[1, 2, 3] = 1848
    d = @m[1]
    assert_equal(1848, d[2, 3])
    assert_equal(2, d.dimensions.size)
    assert_equal(4, d.dimensions[0])
    assert_equal(5, d.dimensions[1])
    d[2, 0] = 1917
    assert_equal(1917, d[2, 0])
    assert_equal(1917, @m[1, 2, 0])
  end

  def test_each_visit_all_elements
    @m.fill(1)

    cpt = 0
    @m.each do |v|
      assert_equal(v, 1)
      cpt += 1
    end

    assert_equal(@m.count, cpt)
  end

  def test_count_return_the_right_number_of_values
    assert_equal(3 * 4 * 5, @m.count)
  end

  def test_each_index_visit_all_elements_once
    @matrices.each { |m| each_index_visit_all_elements_once(m) }
  end

  def each_index_visit_all_elements_once(m)
    m.fill(1)

    cpt = 0
    m.each_index do |*c|
      assert_equal(1, m[*c])
      cpt += 1
    end

    assert_equal(m.count, cpt)
  end

  def test_count_returns_the_number_of_values
    @matrices.each do |m|
      count = m.dimensions.reduce(:*)
      assert_equal(count, m.count)
    end
  end

  def test_each_index_and_each_with_index
    @matrices.each { |m| each_index_and_each_with_index(m) }
  end

  def each_index_and_each_with_index(m)
    # Check index in check access
    elements = 0
    m.each_index do |*index|
      index.each_with_index do |v, i|
        assert(v >= 0 && v < m.dimensions[i])
      end
      m[*index] = index.sum
      elements += 1
    end

    assert_equal(m.count, elements)

    m.each_with_index do |v, *index|
      assert_equal(v, index.sum)
    end
  end

  def test_fill_does_fill_the_matrix
    @m.fill(45)
    @m.each do |v|
      assert_equal(45, v)
    end
  end

  def test_it_throws_exceptions_when_index_are_bad
    assert_raises(MultiMatrix::DimensionError) do
      @m[1, 2, 3, 4]
    end
    assert_raises(MultiMatrix::DimensionError) do
      @m[3, 0, 0]
    end
    assert_raises(MultiMatrix::DimensionError) do
      @m[0, 4, 0]
    end
    assert_raises(MultiMatrix::DimensionError) do
      @m[0, 0, 5]
    end
  end

  def test_is_enumerable
    @m.fill(1)

    @m[1, 2, 3] = 1848
    @m[0, 0, 0] = 1917
    @m[2, 3, 4] = 1789

    assert(@m.any? { |v| v == 1917 })
    assert(@m.include?(1848))
    assert_equal([1917, 1848, 1789], @m.select { |v| v > 10 })
    assert_equal(1917, @m.max)
    assert_equal(1, @m.min)
  end

  def test_each_order_is_lower_rank_first
    @m.fill(0)
    cpt = 0
    @m.dimensions[0].times do |l|
      @m.dimensions[1].times do |c|
        @m.dimensions[2].times do |r|
          @m[l, c, r] = cpt
          cpt += 1
        end
      end
    end

    old = -1
    @m.each do |v|
      assert_equal(old + 1, v)
      old = v
    end
  end

  def test_set
    @m.set((1..@m.count).to_a)
    assert_equal(@m.count, @m[2, 3, 4])
    assert_equal(1, @m[0, 0, 0])
    assert_equal(3, @m[0, 0, 2])
  end

  def test_set_raise_exception_if_dimension_mismatch
    assert_raises(MultiMatrix::DimensionError) do
      @m.set([1, 2, 3, 4])
    end

    assert_raises(MultiMatrix::DimensionError) do
      @m.set((0..@m.count).to_a)
    end
  end

  def test_at_access_by_linear_index
    @m[0, 0, 0] = 26
    assert_equal(26, @m.at(0))

    @m[2, 3, 4] = 26 * 26
    assert_equal(26 * 26, @m.at(@m.count - 1))

    @m[0, 3, 4] = 26 * 26 * 26
    assert_equal(26 * 26 * 26, @m.at(19))
  end

  def test_set_at_set_by_linear_index
    @m.set_at(0, 26)
    assert_equal(26, @m[0, 0, 0])

    @m.set_at(@m.count - 1, 26 * 26)
    assert_equal(26 * 26, @m[2, 3, 4])

    @m.set_at(19, 26 * 26 * 26)
    assert_equal(26 * 26 * 26, @m[0, 3, 4])
  end

  def test_coordinates_by_linear_index
    assert_equal([0, 0, 0], @m.coordinates(0))
    assert_equal([2, 3, 4], @m.coordinates(@m.count - 1))
    assert_equal([0, 3, 4], @m.coordinates(19))
  end

  def test_index_convertion_from_linear_to_index
    assert_equal(0, @m.index(0, 0, 0))
    assert_equal(@m.count - 1, @m.index(2, 3, 4))
    assert_equal(19, @m.index(0, 3, 4))
  end

  def test_both_index_and_at
    @matrices.each do |m|
      m.count.times do |index|
        coordinates = m.coordinates(index)
        assert_equal(index, m.index(*coordinates))
      end
    end
  end

  def test_submatrix
    i = 0
    @m.each_index do |*index|
      @m[*index] = i
      i += 1
    end

    ident = @m.sub_matrix(0..2,0..3,0..4)
    @m.each_index do |i|
      assert_equal( @m.at(i), ident.at(i) )
    end

    m2 = MultiMatrix.build(:int, [5, 5])
    i = 0
    m2.each_index do |*index|
      m2[*index] = i
      i += 1
    end

    small = m2.sub_matrix(1..2,1..2)
    assert_equal(6, small[0,0])
    assert_equal(7, small[0,1])
    assert_equal(11, small[1,0])
    assert_equal(12, small[1,1])

  end

end
