module Visited
  def accept(visitor)
    visitor.visit(self)
  end

  private def accept_elements(visitor, elements)
    elements.each do |element|
      element.accept(visitor)
    end
  end
end

## Visitor. Can inherited (redefine *visit*) or used with a Proc
class Visitor
  def initialize(&code)
    @code = code
  end

  def visit(e)
    @code.call(e) if @code
  end
end
