require_relative '../cryptodag.rb'


def print_graphviz(dag)
    vertices = dag.find_all_nodes()
    puts "digraph {"
    # digraph options
    puts "\tgraph [rankdir=LR];"
    puts "\tnode [shape=box,style=filled, color=lightgray, fontname=\"helvetica\"];"
    puts ""
    # display vertices
    vertices.each() do |e|
        case e.class.name
        when "InputNode"
            puts "\t#{e.name}[color=pink];"
        when "ConstantNode"
            puts "\t#{e.name}[color=palegreen];"
        when "OutputNode"
            puts "\t#{e.name}[color=skyblue];"
        else
            puts "\t#{e.name};"
        end
    end
    puts ""
    # display arcs
    vertices.each() do |v|
        v.inputs.each() do |u|
            puts "\t" + u.name() + " -> " + v.name()
        end
    end
    puts "}"
end

def get_graphviz(dag)
    res = ""
    vertices = dag.find_all_nodes()
    res += "digraph {\n"
    # digraph options
    res +=  "\tgraph [rankdir=LR];\n"
    res +=  "\tnode [shape=box,style=filled, color=lightgray, fontname=\"helvetica\"];\n"
    res +=  "\n"
    # display vertices
    vertices.each() do |e|
        case e.class.name
        when "InputNode"
            res +=  "\t#{e.name}[color=pink];\n"
        when "ConstantNode"
            res +=  "\t#{e.name}[color=palegreen];\n"
        when "OutputNode"
            res +=  "\t#{e.name}[color=skyblue];\n"
        else
            res +=  "\t#{e.name};\n"
        end
    end
    res +=  "\n"
    # display arcs
    vertices.each() do |v|
        v.inputs.each() do |u|
            res +=  "\t" + u.name() + " -> " + v.name() + "\n"
        end
    end
    res +=  "}\n"
end


def writefile_graphviz(dag, filename)
    file = File.open(filename, "w")
    file.puts(get_graphviz(dag))
    file.close
end