require 'set'

require_relative "abstractwriter"
require_relative "../constraints/all"

def extract_name_number(name)
    return name.split("_")[-1].to_i
end

## Diff model implementation
class Midori128Replica < AbstractWriter

    def generate_extended_model(dag, rounds)
        generate_variables(dag, rounds)
        generate_constraints(dag, rounds)
        add_diff_variables(dag, rounds)
        generate_objective(dag, rounds)
    end

    def generate_variables(dag, rounds)
        # transition-helper variables
        @dx = Table.new("DX", "bool", [rounds, 4, 4])
        @model.add_variables(@dx) # DX variables
        @dy = Table.new("DY", "bool", [rounds-1, 4, 4])
        @model.add_variables(@dy) # DY variables
        @dz = Table.new("DZ", "bool", [rounds-1, 4, 4])
        @model.add_variables(@dz) # DZ variables
        @dk = Table.new("DK", "bool", [4,4])
        @model.add_variables(@dk) # DK variables

        # generation variables
        @variable_dict = {}
        for node in dag.find_all_nodes()
            case node
            when ConstantNode
                table = ConstantTable.new(node.name, "int", [node.x, node.y], Array.new(node.x * node.y, 0))
                @model.add_variables(table)
                @variable_dict[node] = table
            else
                table = Table.new(node.name, "bool", [node.x, node.y])
                @model.add_variables(table)
                @variable_dict[node] = table
                case node
                when SubCellNode
                    i = extract_name_number(node.name)
                    table.each_index do |*index|
                        @model.add_constraints(Equality.new(table[*index], @dx[i][*index]))
                    end
                when ShuffleCellNode
                    i = extract_name_number(node.name)
                    table.each_index do |*index|
                        @model.add_constraints(Equality.new(table[*index], @dy[i][*index]))
                    end
                when MixColumnNode
                    i = extract_name_number(node.name)
                    table.each_index do |*index|
                        @model.add_constraints(Equality.new(table[*index], @dz[i][*index]))
                    end
                when InputNode
                    if node.name == "K"
                        table.each_index do |*index|
                            @model.add_constraints(Equality.new(table[*index], @dk[*index]))
                        end
                    end
                end
            end
        end
        @model
    end

    def generate_constraints(dag, n)
        for node in dag.find_all_nodes()
            name = node.name
            modelinputvars = node.inputs.map { |inputnode| @variable_dict[inputnode] }
            outputvar = @variable_dict[node]
            case node
            when ConstantNode
                # nothing to do here
              when InputNode
                # nothing to do here
              when OutputNode
                # input[*] = output[*]
                outputvar.each_index do |*index|
                  @model.add_constraints(
                    Equality.new(modelinputvars[0][*index], outputvar[*index])
                  )
                end
              when KeySeparationNode
                # input[*] = output[*]
                outputvar.each_index do |*index|
                  @model.add_constraints(
                    Equality.new(modelinputvars[0][*index], outputvar[*index])
                  )
                end
              when XorNode
                    # if node.name.split("_")[0] == "roundKey" || node.name.split("_")[0] == "endRound" || node.name=="addKey"
                        outputvar.each_index do |*index|
                        @model.add_constraints(Diff1.new(
                            modelinputvars[0][*index],
                            modelinputvars[1][*index],
                            outputvar[*index]
                        ))
                        end
                    # end
                when SubCellNode
                    outputvar.each_index do |*index|
                    @model.add_constraints(
                        Equality.new(modelinputvars[0][*index], outputvar[*index])
                    )
                end
                when ShuffleCellNode
                # apply permutation input[indices] = output[shuffle(indices)]
                outputvar.each_index do |*index|
                  y,x = *index
                  @model.add_constraints(Equality.new(
                    outputvar[x,y],
                    modelinputvars[0][*node.permuted_indices(*index)],
                  ))
                end
            when MixColumnNode
                coeff_tab = [
                  [[1, 2, 3], 0],
                  [[0, 2, 3], 1],
                  [[0, 1, 3], 2],
                  [[0, 1, 2], 3],
                ]
                outputvar.dimensions[1].times do |dim2|
                  coeff_tab.each do |coeff_line|
                    model.add_constraints(Diff1.new(
                      *(coeff_line[0].map { |coeff_input| modelinputvars[0][coeff_input, dim2] } + [outputvar[coeff_line[1], dim2]])
                    ))
                  end
                end
                v = modelinputvars[0]
                w = outputvar
                4.times do |j| 
                  model.add_constraints(IncludedIn.new(
                    Sum.new(
                      v[0,j],v[1,j],v[2,j],v[3,j], w[0,j],w[1,j],w[2,j],w[3,j]
                    ),
                    Set[0,4,5,6,7,8]
                  ))
                end
                4.times do |j|
                  model.add_constraints(Equivalence.new(
                    Equals.new(Sum.new(v[0,j],v[1,j],v[2,j],v[3,j]), 0),
                    Equals.new(Sum.new(w[0,j],w[1,j],w[2,j],w[3,j]), 0)
                  ))
                end
            else
                raise "Node #{node.name} have not been covered by constraints"
            end
        end




        # add round key
        @model.add_comment("ADD ROUND KEY")
        (n-1).times do |r|
            @model.add_comment("round #{r}")
            4.times do |j|
                4.times do |i|
                    @model.add_constraints(Diff1.new(
                        @dz[r][i][j],
                        @dk[i][j],
                        @dx[r+1,i,j]
                    ))
                end
            end
        end
        # shuffle calls
        # get shufflecell node to get permutation
        shuffle_node = dag.find_all_nodes().filter {|n| n.class == ShuffleCellNode }[0]
        @model.add_comment("SHUFFLE CELL")
        (n-1).times do |r|
            @model.add_comment("round #{r}")
            4.times do |j|
                4.times do |i|
                    si,sj = *shuffle_node.permuted_indices(j,i)
                    @model.add_constraints(Equality.new(
                        @dy[r][i][j],
                        @dx[r][si][sj]
                    ))
                end
            end
        end
        # mix columns
        @model.add_comment("MIX COLUMNS")
        (n-1).times do |r|
            @model.add_comment("round #{r}")
            4.times do |j|
                @model.add_constraints(Diff1.new(@dy[r][1][j], @dy[r][2][j],@dy[r][3][j],@dz[r][0][j]))
                @model.add_constraints(Diff1.new(@dy[r][0][j], @dy[r][2][j],@dy[r][3][j],@dz[r][1][j]))
                @model.add_constraints(Diff1.new(@dy[r][0][j], @dy[r][1][j],@dy[r][3][j],@dz[r][2][j]))
                @model.add_constraints(Diff1.new(@dy[r][0][j], @dy[r][1][j],@dy[r][2][j],@dz[r][3][j]))
            end
        end
        @model.add_comment("MIX COLUMNS (MDS)")
        (n-1).times do |r|
            4.times do |j|
                @model.add_constraints(IncludedIn.new(
                    Sum.new(
                        @dy[r][0][j], @dy[r][1][j], @dy[r][2][j], @dy[r][3][j],
                        @dz[r][0][j], @dz[r][1][j], @dz[r][2][j], @dz[r][3][j]
                    ),
                    Set[0,4,5,6,7,8]
                ))
            end
        end
        (n-1).times do |r|
            4.times do |j|
                @model.add_constraints(Equivalence.new(
                    Equals.new(Sum.new(@dy[r][0][j], @dy[r][1][j], @dy[r][2][j], @dy[r][3][j]), 0),
                    Equals.new(Sum.new(@dz[r][0][j], @dz[r][1][j], @dz[r][2][j], @dz[r][3][j]), 0)
                ))
            end
        end

        
        @model
    end

    ## adds diff variables (for each couple of mixcolumn states columns, diff in the column position)
    def add_diff_variables(dag, n)
        # add diff variables
        @diffz = Table.new("DIFF_Z", "bool", [n-1, 4, 4, n-1])
        @model.add_variables(@diffz) # DIFF_Z variables
        @diffy = Table.new("DIFF_Y", "bool", [n-1, 4, 4, n-1])
        @model.add_variables(@diffy) # DIFF_Y variables
        # diff constraints
        @model.add_comment("DIFF VARIABLES LINK BETWEEN ORIGINAL VARIABLES")
        (n-1).times do |i1|
            4.times do |j|
                4.times do |k|
                    (n-1).times do |i2|
                        @model.add_constraints(Diff1.new(@dy[i1][j][k], @dy[i2][j][k], @diffy[i1][j][k][i2]))
                        @model.add_constraints(Diff1.new(@dz[i1][j][k], @dz[i2][j][k], @diffz[i1][j][k][i2]))
                        @model.add_constraints(Diff1.new(@dx[i1+1][j][k], @dx[i2+1][j][k], @diffz[i1][j][k][i2]))
                    end
                end
            end
        end
        @model.add_comment("DIFF VARIABLES SYMMETRY & TRANSITIVITY")
        (n-1).times do |i1|
            4.times do |j|
                4.times do |k|
                    (n-1).times do |i2|
                        @model.add_constraints(Equality.new(@diffz[i1][j][k][i2], @diffz[i2][j][k][i1]))
                        @model.add_constraints(Equality.new(@diffy[i1][j][k][i2], @diffy[i2][j][k][i1]))
                        (n-1).times do |i3| # transitivity
                            if i3 > i2 
                                @model.add_constraints(Diff1.new(
                                    @diffz[i1][j][k][i3],
                                    @diffz[i1][j][k][i2],
                                    @diffz[i2][j][k][i3]
                                ))
                                @model.add_constraints(Diff1.new(
                                    @diffy[i1][j][k][i3],
                                    @diffy[i1][j][k][i2],
                                    @diffy[i2][j][k][i3]
                                ))
                            end
                        end
                    end
                end
            end
        end
        @model.add_comment("DIFF VARIABLES MIX COLUMNS")
        (n-1).times do |i1|
            4.times do |k|
                i1.times do |i2|
                    @model.add_constraints(IncludedIn.new(
                        # sum(j in 0..3)(DIFF_Y[i1,j,k, i2]+DIFF_Z[i1,j,k, i2]) 
                        Sum.new(
                            @diffy[i1][0][k][i2], @diffy[i1][1][k][i2], @diffy[i1][2][k][i2], @diffy[i1][3][k][i2],
                            @diffz[i1][0][k][i2], @diffz[i1][1][k][i2], @diffz[i1][2][k][i2], @diffz[i1][3][k][i2]
                        ),
                        Set[0,4,5,6,7,8]
                    ))
                end
            end
        end

        @model
    end

    def generate_objective(dag, rounds)
        @model.add_comment("OBJECTIVE RELATED CONSTRAINTS AND ANNOTATIONS")
        varlist = dag.find_all_nodes().filter { |node|
            node.class == SubCellNode
        }.map { |node|
            @variable_dict[node].to_a()
        }.flatten()
        obj = Variable.new("obj", 1..64)
        @model.add_variables(obj)
        @model.add_constraints(Equality.new(obj, rounds))
        @model.add_constraints(
            Equality.new(obj, TableSum.new(@dx, ["r","i","j"]))
        )
        @model.add_instructions(Instruction.new("solve satisfy"))
        # original_var_list = []
        # rounds.times do |r|
        #     4.times do |x|
        #         4.times do |y|
        #             original_var_list.append("DX[#{r},#{x},#{y}]")
        #         end
        #     end
        # end
        # original_var_list_text = original_var_list.join(", ")
        # @model.add_instructions(Instruction.new("solve::seq_search([
        #     int_search([obj], input_order, indomain_min, complete),
        #     int_search(
        #         [obj]++
        #         [#{original_var_list_text}]
        #         ,dom_w_deg, indomain_min, complete
        #     )
        # ]) satisfy"))
        return @model
    end

end
