require_relative '../cryptodag.rb'


##
# workflow:
# 1. generate variables (call generate_variables)
# 2. generate constraints (call generate_constraints)
# 3. possibly create minizinc model string (puts MinizincWriter.new.get_model(model))
#
class AbstractWriter
    attr_reader :model
    attr_reader :variable_dict

    def initialize()
        @model = Model.new()
        @variable_dict = {} # variable_dict[DAGnode] = table_output
    end

    def generate_model(dag)
        generate_variables(dag)
        generate_constraints(dag)
        generate_objective(dag)
    end

    ## constructs variables and @variable_dict
    def generate_variables(dag)
        vertices = dag.find_all_nodes()
        vertices.each() do |e|
            add_node_variables(e)
        end
    end

    def generate_constraints(dag)
        vertices = dag.find_all_nodes()
        vertices.each() do |e|
            add_node_constraints(e)
        end
        @model
    end

    protected
    ## abstract method (process each node of the DAG)
    def add_node_variables(node)
    end

    def add_node_constraints(node)
    end

    def generate_objective(dag)
    end
end