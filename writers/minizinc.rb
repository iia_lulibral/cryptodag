require_relative '../cryptodag.rb'

def print_minizinc_dk_step1opt(dag, mc_improvements=true)
    puts get_minizinc_dk_step1opt(dag, mc_improvements)
end

def forbid_assign(vars, assign)
    res = "0"
    (vars.length).times do |i|
        if assign[i] == 1
            res += "+#{vars[i]}"
        else
            res += "+(1-#{vars[i]})"
        end
    end
    res += " <= #{vars.length-1}"
    return res
end

## Minizinc writer. Produces a mzn file with
def get_minizinc_dk_step1opt(dag, mc_improvements=true)
    res = ""
    res += "%%%%% AUTOMATICALLY GENERATED USING CryptoDag (https://gitlab.limos.fr/iia_lulibral/cryptodag)\n"
    res += "\n"
    vertices = dag.find_all_nodes()
    # create variables representing vertices
    res += "%%%%% Cryptosystem states (represented as 4x4 boolean matrices)\n"
    vertices.each() do |e|
        name = e.name
        res += "array[0..3, 0..3] of var 0..1: #{name};\n"
    end
    res += "\n"
    res += "\n"
    # create constraints for all crypto node
    res += "%%%%% Cryptosystem constraints (link states together)\n"
    vertices.each() do |e|
        res += "% #{e.class.name} (#{e.name})\n"
        case e.class.name
        when "ConstantNode"
            res += "% sets values to 0\n"
            res += "constraint forall(i in 0..3, j in 0..3)(#{e.name}[i,j] = 0);\n"
        when "InputNode"
            res += "% do nothing (no constraints)\n"
        when "OutputNode"
            inputname = e.inputs[0].name
            res += "constraint forall(i in 0..3, j in 0..3)(#{e.name}[i,j] = #{inputname}[i,j]);\n"
        when "KeySeparationNode"
            inputname = e.inputs[0].name
            res += "constraint forall(i in 0..3, j in 0..3)(#{e.name}[i,j] = #{inputname}[i,j]);\n"
        when "XorNode"
            # diff1 linearization
            vars = e.inputs.map{|i| "#{i.name}[i,j]"}
            vars.push("#{e.name}[i,j]")
            vars.each do |v|
                res += "constraint forall(i in 0..3, j in 0..3)(-#{vars.join("-")} + 2*#{v} <= 0);\n"
            end
            inputsum = e.inputs.map{|i| "#{i.name}[i,j]"}.join("+")
            res += "constraint forall(i in 0..3, j in 0..3)(#{inputsum}+#{e.name}[i,j] != 1);\n"
        when "SubCellNode"
            inputname = e.inputs[0].name
            res += "constraint forall(i in 0..3, j in 0..3)(#{e.name}[i,j] = #{inputname}[i,j]);\n"
        when "ShuffleCellNode"
            inputname = e.inputs[0].name
            4.times() do |i|
                4.times() do |j|
                    si,sj = e.permuted_indices(i,j)
                    res += "constraint #{e.name}[#{i},#{j}] = #{inputname}[#{si},#{sj}];\n" # should be correct but do not correspond to the mzn model
                    # puts "constraint #{e.name}[#{j},#{i}] = #{inputname}[#{si},#{sj}];" # corresponds to the mzn model
                end
            end
        when "MixColumnNode"
            a = e.inputs[0].name
            b = e.name
            # diff1 linearization
            vars = ["#{a}[1,j]","#{a}[2,j]","#{a}[3,j]","#{b}[0,j]"]
            vars.each do |v|
                res += "constraint forall(i in 0..3, j in 0..3)(-#{vars.join("-")} + 2*#{v} <= 0);\n"
            end
            vars = ["#{a}[0,j]","#{a}[2,j]","#{a}[3,j]","#{b}[1,j]"]
            vars.each do |v|
                res += "constraint forall(i in 0..3, j in 0..3)(-#{vars.join("-")} + 2*#{v} <= 0);\n"
            end
            vars = ["#{a}[0,j]","#{a}[1,j]","#{a}[3,j]","#{b}[2,j]"]
            vars.each do |v|
                res += "constraint forall(i in 0..3, j in 0..3)(-#{vars.join("-")} + 2*#{v} <= 0);\n"
            end
            vars = ["#{a}[0,j]","#{a}[1,j]","#{a}[2,j]","#{b}[3,j]"]
            vars.each do |v|
                res += "constraint forall(i in 0..3, j in 0..3)(-#{vars.join("-")} + 2*#{v} <= 0);\n"
            end
            # splitted constraint
            # res += "constraint forall(j in 0..3)(#{a}[1,j] + #{a}[2,j] + #{a}[3,j] + #{b}[0,j] != 1);\n"
            # res += "constraint forall(j in 0..3)(#{a}[0,j] + #{a}[2,j] + #{a}[3,j] + #{b}[1,j] != 1);\n"
            # res += "constraint forall(j in 0..3)(#{a}[0,j] + #{a}[1,j] + #{a}[3,j] + #{b}[2,j] != 1);\n"
            # res += "constraint forall(j in 0..3)(#{a}[0,j] + #{a}[1,j] + #{a}[2,j] + #{b}[3,j] != 1);\n"
            # and constraint
            # puts "constraint forall(j in 0..3)("
            # puts "\t#{a}[1,j] + #{a}[2,j] + #{a}[3,j] + #{b}[0,j] != 1 /\\"
            # puts "\t#{a}[0,j] + #{a}[2,j] + #{a}[3,j] + #{b}[1,j] != 1 /\\"
            # puts "\t#{a}[0,j] + #{a}[1,j] + #{a}[3,j] + #{b}[2,j] != 1 /\\"
            # puts "\t#{a}[0,j] + #{a}[1,j] + #{a}[2,j] + #{b}[3,j] != 1"
            # puts ");"
            if mc_improvements
                res += "% MixColumn crypto improvements (MDS property)\n"
                ## first part
                res += "constraint forall(j in 0..3) (\n";
                res += "\tsum(i in 0..3)(#{a}[i,j]+#{b}[i,j]) in {0,4,5,6,7,8}\n"
                res += ");\n"

                vars = ["#{a}[0,j]","#{a}[1,j]","#{a}[2,j]","#{a}[3,j]","#{b}[0,j]","#{b}[1,j]","#{b}[2,j]","#{b}[3,j]"]
                # # sum != 1
                # 8.times do |i|
                #     assign = [0,0,0,0,0,0,0,0]
                #     assign[i] = 1
                #     res += "constraint forall(j in 0..3)(#{forbid_assign(vars, assign)});\n";
                # end
                # # sum != 2
                # 8.times do |i|
                #     i.times do |j|
                #         assign = [0,0,0,0,0,0,0,0]
                #         assign[i] = 1
                #         assign[j] = 1
                #         res += "constraint forall(j in 0..3)(#{forbid_assign(vars, assign)});\n";
                #     end
                # end
                # # sum != 3
                # 8.times do |i|
                #     i.times do |j|
                #         j.times do |k|
                #             assign = [0,0,0,0,0,0,0,0]
                #             assign[i] = 1
                #             assign[j] = 1
                #             assign[k] = 1
                #             res += "constraint forall(j in 0..3)(#{forbid_assign(vars, assign)});\n";
                #         end
                #     end
                # end

                ## second part
                # for i in [0,1]
                #     for j in [0,1]
                #         for k in [0,1]
                #             for l in [0,1]
                #                 if i+j+k+l > 0
                #                     res += "constraint forall(j in 0..3)(#{forbid_assign(vars, [0,0,0,0, i,j,k,l])});\n";
                #                     res += "constraint forall(j in 0..3)(#{forbid_assign(vars, [i,j,k,l, 0,0,0,0])});\n";
                #                 end
                #             end
                #         end
                #     end
                # end
                res += "constraint forall(j in 0..3) (\n";
                res += "\tsum(i in 0..3)(#{b}[i,j])==0 <-> sum(i in 0..3)(#{a}[i,j])==0\n"
                res += ");\n"
            end
        else
            raise "print_minizinc_dk_step1opt: node not implemented (#{e.class.name})\n"
        end
        res += "\n"
    end
    res += "\n"
    res += "\n"
    res += "%%%%% Objective and solving process\n"
    res += "var 1..20: objective;\n"
    # gets all subCell nodes
    sboxes = []
    vertices.each() do |e|
        if e.class.name == "SubCellNode"
            sboxes.push(e)
        end
    end
    res += "constraint objective=sum(i in 0..3, j in 0..3)(#{sboxes.map{|i| "#{i.name}[i,j]"}.join(" + ")});\n"
    res += "solve minimize objective;\n"
end
