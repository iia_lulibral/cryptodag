require_relative '../cryptodag.rb'
require_relative '../operators/elementary.rb'
require_relative '../constraints/all.rb'

require_relative './graphviz_atomic.rb'
require_relative './dag_shaving.rb'


##
# returns new xors that can be used to improve the relaxations
# @param atoms [Vec<CryptoAtom=Variable>]
# @param operators [Vec<CryptoOperators>]
# @param to_shave [Set<CryptoAtom|CryptoOperator>]
# @return [new_atoms, new_operators]
def generate_xors(atoms, operators, custom_xors, max_size=4)
    initial_xors = Set.new(operators.filter{|op| op.class <= XorOperator}.map{|op| Set.new(op.inputs+op.outputs)}+custom_xors)
    new_xors = initial_xors.dup
    variables = Set.new()
    variable_adj = {}
    new_xors.each do |op|
        op.each do |v|
            variables.add(v)
            variable_adj[v] = variable_adj.fetch(v, Set.new()).add(op)
        end
    end
    xor_to_check = new_xors.dup
    puts "GENERATE_XORS(limit=#{max_size}): with #{new_xors.length} initial xors"
    to_continue = true
    while to_continue
        to_continue = false
        new_xor_to_check = Set.new()
        variables.each do |v|
            variable_adj[v].each do |a|
                if xor_to_check.include?(a)
                    variable_adj[v].each do |b|
                        if a != b
                            c = (a+b)-(a&b)
                            if c.length >= 1 && c.length <= max_size
                                nb_diffs = c.filter{|v| v.name.name.start_with?("diff_")}.length
                                if nb_diffs <= 1 && !new_xors.include?(c)
                                # if !new_xors.include?(c)
                                    new_xors.add(c)
                                    new_xor_to_check.add(c)
                                    to_continue = true
                                    c.each do |v|
                                        variable_adj[v] = variable_adj.fetch(v, Set.new()).add(c)
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        xor_to_check = new_xor_to_check
        puts "\tround finished with: #{new_xors.length} clauses"
    end
    return new_xors
end


def create_abstract_model(atoms, operators, solve_instructions, mds_set=Set[0,4,5,6,7,8])
    model = Model.new()
    variable_dict = {} # Atom -> Variable
    # add atoms as booleans
    atoms.each do |atom|
        var = Variable.new(atom.name, 0..1, atom.value != nil ? 0 : nil)
        variable_dict[atom] = var
        model.add_variables(var)
    end
    # add constraints
    xor_clauses = []
    operators.each do |op|
        case op.class.name
        when "XorOperator"
            xor_clauses.push(Set.new(
                op.inputs.map {|atom| variable_dict[atom]}+
                op.outputs.map {|atom| variable_dict[atom]}
            ))
        when "SOperator"
            model.add_constraints(Equality.new(
                *op.inputs.map {|atom| variable_dict[atom]},
                *op.outputs.map {|atom| variable_dict[atom]}
            ))
        when "EqualityOperator"
            model.add_constraints(Equality.new(
                *op.inputs.map {|atom| variable_dict[atom]},
                *op.outputs.map {|atom| variable_dict[atom]}
            ))
        when "MixColumnsOperator"
            model.add_constraints(IncludedIn.new(
                Sum.new(
                    *op.inputs.map {|atom| variable_dict[atom]},
                    *op.outputs.map {|atom| variable_dict[atom]}
                ),
                mds_set
            ))
            model.add_constraints(Equivalence.new(
                Equals.new(Sum.new(*op.inputs.map {|atom| variable_dict[atom]}), 0),
                Equals.new(Sum.new(*op.outputs.map {|atom| variable_dict[atom]}), 0)
            ))
        else
            # p c.class
            raise "gen_abstract_constraint_model: unknown operator: #{op.class.name}"
        end
    end
    # add objective function
    s_operators = operators.filter {|op| op.class <= SOperator}
    obj = Variable.new("obj", 1..64)
    model.add_variables(obj)
    model.add_constraints(
      Equality.new(
        obj,
        Sum.new(*s_operators.map{|op| variable_dict[op.outputs[0]]})
      )
    )
    model.add_instructions(Instruction.new(solve_instructions))

    return [model,variable_dict,xor_clauses]
end


##
# 
#
def add_diff_variables_mixcolumn_lines(model, atoms, operators, atom_to_variable, mds_set=Set[0,4,5,6,7,8])
    res_xors = []
    mc_operators = operators.filter{|op| op.class <= MixColumnsOperator}
    op_to_ids = {}
    ids_to_op = {}
    id = 0
    mc_operators.each do |op|
        op_to_ids[op] = id
        ids_to_op[id] = op
        id += 1
    end
    puts "#{mc_operators.length} MIX_COLUMNS OPERATORS"
    puts "ADD_DIFF (MIXCOLUMN LINES)"
    puts "\t#{mc_operators.length} mixColumns operators found"
    diffs = []
    mc_operators.each do |o1|
        mc_operators.each do |o2|
            if op_to_ids[o1] < op_to_ids[o2]
                4.times do |row|
                    diffs.push([op_to_ids[o1],op_to_ids[o2],row,"I"])
                    diffs.push([op_to_ids[o1],op_to_ids[o2],row,"O"])
                end
            end
        end
    end
    diffs_to_var = {}
    puts "\tadding #{diffs.length} diff variables"
    diffs.each do |d|
        o1,o2,row,t = d[0],d[1],d[2],d[3],d[4]
        diffs_to_var[d] = Variable.new("diff_m#{o1}_m#{o2}_l#{row}_#{t}", 0..1)
        model.add_variables(diffs_to_var[d])
    end
    # add linking to original variables
    diffs.each do |d|
        o1,o2,row,t = d[0],d[1],d[2],d[3]
        if t == "I"
            res_xors.push(Set.new([
                ids_to_op[o1].inputs[row],
                ids_to_op[o2].inputs[row],
                diffs_to_var[d]
            ]))
        else
            res_xors.push(Set.new([
                ids_to_op[o1].outputs[row],
                ids_to_op[o2].outputs[row],
                diffs_to_var[d]
            ]))
        end
    end
    # add transitivity
    mc_operators.each do |o1|
        mc_operators.each do |o2|
            mc_operators.each do |o3|
                if o1 != o2 && o2 != o3 && o1 != o3
                    id1, id2, id3 = op_to_ids[o1], op_to_ids[o2], op_to_ids[o3]
                    if id1 < id2 && id2 < id3
                        4.times do |row|
                            ["I","O"].each do |t|
                                vars = [
                                    diffs_to_var[[id1,id2,row,t]],
                                    diffs_to_var[[id1,id3,row,t]],
                                    diffs_to_var[[id2,id3,row,t]]
                                ]
                                # model.add_constraints(Diff1.new(*vars))
                                res_xors.push(Set.new(vars))
                            end
                        end
                    end
                end
            end
        end
    end
    # add mix columns property (sum diffs input + diffs output) for each couple of operators
    mc_operators.each do |o1|
        mc_operators.each do |o2|
            id1, id2 = op_to_ids[o1], op_to_ids[o2]
            if id1 < id2
                model.add_constraints(IncludedIn.new(
                    Sum.new(
                        *( 4.times.map{|row| diffs_to_var[[id1,id2,row,"I"]]} + 4.times.map{|row| diffs_to_var[[id1,id2,row,"O"]]} )
                    ), mds_set
                ))
                model.add_constraints(Equivalence.new(
                    Equals.new(Sum.new(*4.times.map{|row| diffs_to_var[[id1,id2,row,"I"]]}), 0),
                    Equals.new(Sum.new(*4.times.map{|row| diffs_to_var[[id1,id2,row,"O"]]}), 0)
                ))
            end
        end
    end
    return res_xors
end