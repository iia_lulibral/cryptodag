require 'union_find'

require_relative '../operators/elementary.rb'

### DIRTY HACK (modify union_find datastructure)
class UnionFind::UnionFind
    public :find_root
end
##############


def compute_precedences(atoms, operators)
    preds = {}
    succs = {}
    operators.each do |op|
        op.inputs.each do |inp|
            preds[op] = preds.fetch(op, []).push(inp)
            succs[inp] = succs.fetch(inp, []).push(op)
        end
        op.outputs.each do |out|
            preds[out] = preds.fetch(out, []).push(op)
            succs[op] = succs.fetch(op, []).push(out)
        end
    end
    return [preds, succs]
end


##
# applies various operations to the DAG
#
def shave_dag(atoms, operators)
    puts "SHAVING DAG : #{atoms.length} atoms and #{operators.length} operators"
    nb_atoms,nb_operators = 0,0
    new_atoms, new_operators = atoms, operators
    while nb_atoms != new_atoms.length && nb_operators != new_operators.length
        nb_atoms,nb_operators = new_atoms.length, new_operators.length
        # puts "nb atoms: #{new_atoms.length}\tnb operators: #{new_operators.length}"
        new_atoms, new_operators = *shave_end_dag(new_atoms,new_operators)
        new_atoms, new_operators = *shave_constants_dag(new_atoms,new_operators)
        new_atoms, new_operators = *shave_simple_input_xor(new_atoms,new_operators)
        new_atoms, new_operators = *shave_equalities(new_atoms,new_operators)
        # new_atoms, new_operators = *shave_intermediate_xors(new_atoms,new_operators)
        writefile_graphviz(new_atoms, new_operators, "debug_shaving.dot")
        puts "\t#{nb_atoms-new_atoms.length} atoms removed"
        puts "\t#{nb_operators-new_operators.length} operators removed"
        puts "\t-------"
        
    end
    puts "FINISHED SHAVING : #{new_atoms.length} atoms and #{new_operators.length} operators"
    puts ""
    return [new_atoms, new_operators]
end



##
# removes operators and atoms to be shaved and returns a new dag
# @param atoms [Vec<CryptoAtom=Variable>]
# @param operators [Vec<CryptoOperators>]
# @param to_shave [Set<CryptoAtom|CryptoOperator>]
# @return [new_atoms, new_operators]
def get_dag_subset(atoms, operators, to_shave)
    # create new dag with shaved nodes
    atom_map = {}
    new_atoms = []
    # create new set of atoms
    atoms.each do |atom|
        if !to_shave.include?(atom)
            new_atom = Variable.new(atom.name, 0..1, value=atom.value)
            atom_map[atom] = new_atom
            new_atoms.push(new_atom)
        end
    end
    # create new set of operators
    new_operators = []
    operators.each do |op|
        if !to_shave.include?(op)
            new_inputs = op.inputs.filter{|e| !to_shave.include?(e)}.map{|e| atom_map[e]}
            new_outputs = op.outputs.filter{|e| !to_shave.include?(e)}.map{|e| atom_map[e]}
            new_op = op.dup
            # p "#{op.class <= XorOperator}\t#{new_inputs.length}"
            if (op.class <= XorOperator) && (new_inputs.length == 1)
                # p "###"
                raise "get_dag_subset: node with multiple outputs detected #{op}" unless new_outputs.length == 1
                new_op = EqualityOperator.new(new_inputs[0], new_outputs[0])
            else
                new_op.inputs = new_inputs
                new_op.outputs = new_outputs
            end
            new_operators.push(new_op)
        end
    end
    return [new_atoms, new_operators]
end


##
# simplifies the dag
# @param atoms [Vec<CryptoAtom=Variable>]
# @param operators [Vec<CryptoOperators>]
# @return [new_atoms, new_operators]
def shave_simple_input_xor(atoms, operators)
    # compute precedence and successors relations
    preds, succs = *compute_precedences(atoms, operators)
    # get input nodes with 1 output which is a xor
    inputs = atoms.filter do |n| 
        preds.fetch(n, []).length == 0 &&
        succs.fetch(n, []).length == 1 &&
        succs.fetch(n, [])[0].class <= XorOperator
    end
    # for each input, replace the xor by
    to_shave = Set.new()
    inputs.each do |atom|
        to_shave.add(atom)
        to_shave.add(succs[atom][0])
    end
    return get_dag_subset(atoms, operators, to_shave)
end

##
# removes constants from the input nodes
# @param atoms [Vec<CryptoAtom=Variable>]
# @param operators [Vec<CryptoOperators>]
# @return [new_atoms, new_operators]
def shave_constants_dag(atoms, operators)
    # compute precedence and successors relations
    preds, succs = *compute_precedences(atoms, operators)
    # get input nodes (should be CryptoAtoms)
    constants = atoms.filter{ |n| n.value != nil }
    to_shave = Set.new() # nodes to remove in the shaved model
    constants.each do |c|
        to_shave.add(c)
    end
    return get_dag_subset(atoms, operators, to_shave)
end


##
# removes end operators and atoms before S-boxes
# @param atoms [Vec<CryptoAtom=Variable>]
# @param operators [Vec<CryptoOperators>]
# @return [new_atoms, new_operators]
def shave_end_dag(atoms, operators)
    # compute precedence and successors relations
    preds, succs = *compute_precedences(atoms, operators)
    # get input nodes (should be CryptoAtoms)
    outputs = atoms.filter{ |n| succs.fetch(n, []).empty? }
    to_shave = Set.new() # nodes to remove in the shaved model
    outputs.each do |output_atom|
        if preds.fetch(output_atom,[]).length == 0
            to_shave.add(output_atom)
        elsif preds[output_atom].length == 1
            op = preds[output_atom][0]
            # if op is not a SOperator and has only one output, the atom (and operator) can be shaved
            if !(op.class <= SOperator) && (succs[op].length == 1)
                to_shave.add(output_atom)
                to_shave.add(op)
            end
        else
            raise "shave_end_dag: output #{output_atom.name.name} has too many preds (#{preds[output_atom]} should be <= 1)"
        end
    end
    return get_dag_subset(atoms, operators, to_shave)
end



##
# simplifies the dag by removing equalities
# @param atoms [Vec<CryptoAtom=Variable>]
# @param operators [Vec<CryptoOperators>]
# @return [new_atoms, new_operators]
def shave_equalities(atoms, operators)
    union_find = UnionFind::UnionFind.new(Set.new(atoms))
    untouched_atoms = Set.new(atoms)
    operators.filter{|op| op.class <= EqualityOperator}.each do |op|
        union_find.union(op.inputs[0], op.outputs[0])
        untouched_atoms.delete(op.inputs[0])
        untouched_atoms.delete(op.outputs[0])
    end
    old_to_root = {}
    root_set = Set.new()
    atoms.each do |a|
        root_set.add(union_find.find_root(a))
        old_to_root[a] = union_find.find_root(a)
    end
    raise "union find: inconsistent results" unless root_set.length == union_find.count_isolated_components
    root_to_new = {}
    new_atoms = []
    root_set.each do |a|
        new_a = Variable.new(a.name, 0..1, value=a.value)
        root_to_new[a] = new_a
        new_atoms.push(new_a)
    end
    # puts "ELIMINATE EQUALITIES (vars: #{atoms.length} -> #{new_atoms.length})"
    # define operators
    new_operators = []
    operators.filter{|op| !(op.class <= EqualityOperator)}.each do |op|
        res = op.dup
        res.inputs = op.inputs.map{|a| root_to_new[old_to_root[a]]}
        res.outputs = op.outputs.map{|a| root_to_new[old_to_root[a]]}
        raise "shave_equalities: different size between new and old input op #{res.inputs.length} should be #{op.inputs.length}" unless res.inputs.length == op.inputs.length 
        raise "shave_equalities: different size between new and old outputs op" unless res.outputs.length == op.outputs.length
        res.inputs.each do |a|
            raise "atom non existing in new_atoms set (input of #{res}, #{a})" unless Set.new(new_atoms).include?(a)
        end
        new_operators.push(res)
    end
    return [new_atoms, new_operators]
end



##
# simplifies the dag by removing intermediate variables between xors (variables that have )
#
def shave_intermediate_xors(atoms, operators)
    # copy atoms and operators
    to_new_atom = {}
    new_atoms = []
    atoms.each do |a|
        new_a = Variable.new(a.name, 0..1, value=a.value)
        to_new_atom[a] = new_a
        new_atoms.push(new_a)
    end
    new_operators = []
    operators.each do |op|
        new_op = op.dup
        new_op.inputs = op.inputs.map{|a| to_new_atom[a]}
        new_op.outputs = op.outputs.map{|a| to_new_atom[a]}
        new_operators.push(new_op)
    end
    preds, succs = *compute_precedences(new_atoms, new_operators)
    to_remove = Set.new()
    # for every XOR operator that has its output linked to xors only
    new_operators.each do |op|
        if op.class <= XorOperator
            raise "shave_intermediate_xors: op should have only one output" unless op.outputs.length == 1
            a = op.outputs[0]
            if succs.fetch(a, []).filter{|s| !(s.class <= XorOperator)}.length == 0
                # delete a, delete op
                to_remove.add(a)
                to_remove.add(op)
                succs.fetch(a, []).each do |s|
                    s.inputs = s.inputs.filter{|a2| a2!=a}+op.inputs
                    s.inputs = s.inputs.uniq
                end
            end
        end
    end
    # remove atoms and operators that should be removed
    new_atoms = new_atoms.filter{|a| !to_remove.include?(a)}
    new_operators = new_operators.filter{|op| !to_remove.include?(op)}
    return [new_atoms, new_operators]
end