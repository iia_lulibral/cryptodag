require "set"

require_relative "abstractwriter"
require_relative "../constraints/all"

class Step1OptConstraintWriter < AbstractWriter
  def add_node_variables(node)
    if node.kind_of?(ConstantNode)
      table = DTable.new(node.name, "int", [node.y, node.x], Array.new(node.x * node.y, 0), constant: true)
    else
      table = DTable.new(node.name, "bool", [node.y, node.x])
    end

    @variable_dict[node] = table
    @model.add_variables(table)
    @model.add_variables(*table.variables)
  end

  def add_node_constraints(node)
    name = node.name
    modelinputvars = node.inputs.map { |inputnode| @variable_dict[inputnode] }
    outputvar = @variable_dict[node]
    case node
    when ConstantNode
      # nothing to do here
    when InputNode
      # nothing to do here
    when OutputNode
      # input[*] = output[*]
      outputvar.each_index do |*index|
        @model.add_constraints(
          Equality.new(modelinputvars[0][*index], outputvar[*index])
        )
      end
    when KeySeparationNode
      # input[*] = output[*]
      outputvar.each_index do |*index|
        @model.add_constraints(
          Equality.new(modelinputvars[0][*index], outputvar[*index])
        )
      end
    when XorNode
      outputvar.each_index do |*index|
        @model.add_constraints(Diff1.new(
          modelinputvars[0][*index],
          modelinputvars[1][*index],
          outputvar[*index]
        ))
      end
    when SubCellNode
      # input[*] = output[*]
      outputvar.each_index do |*index|
        @model.add_constraints(
          Equality.new(modelinputvars[0][*index], outputvar[*index])
        )
      end
    when ShuffleCellNode
      # apply permutation input[indices] = output[shuffle(indices)]
      outputvar.each_index do |*index|
        @model.add_constraints(Equality.new(
          modelinputvars[0][*index],
          outputvar[*node.permuted_indices(*index)]
        ))
      end
    when MixColumnNode
      coeff_tab = [
        [[1, 2, 3], 0],
        [[0, 2, 3], 1],
        [[0, 1, 3], 2],
        [[0, 1, 2], 3],
      ]
      outputvar.dimensions[1].times do |dim2|
        coeff_tab.each do |coeff_line|
          model.add_constraints(Diff1.new(
            *(coeff_line[0].map { |coeff_input| modelinputvars[0][coeff_input, dim2] } + [outputvar[coeff_line[1], dim2]])
          ))
        end
      end
      v = modelinputvars[0]
      w = outputvar
      4.times do |j|
        model.add_constraints(IncludedIn.new(
          Sum.new(
            v[0, j], v[1, j], v[2, j], v[3, j], w[0, j], w[1, j], w[2, j], w[3, j]
          ),
          Set[0, 4, 5, 6, 7, 8]
        ))
      end
      4.times do |j|
        model.add_constraints(Equivalence.new(
          Equals.new(Sum.new(v[0, j], v[1, j], v[2, j], v[3, j]), 0),
          Equals.new(Sum.new(w[0, j], w[1, j], w[2, j], w[3, j]), 0)
        ))
      end
      # res += "constraint forall(j in 0..3) (\n";
      # res += "\tsum(i in 0..3)(#{b}[i,j])==0 <-> sum(i in 0..3)(#{a}[i,j])==0\n"
      # res += ");\n"
    else
      raise "ConstraintWriter.process_node: unknown node: #{node.class}"
    end
    return @model
  end

  def generate_objective(dag)
    varlist = dag.find_all_nodes().filter { |node|
      node.class == SubCellNode
    }.map { |node|
      @variable_dict[node].to_a()
    }.flatten()
    obj = Variable.new("obj", 1..20)
    @model.add_variables(obj)
    @model.add_constraints(
      Equality.new(
        obj,
        Sum.new(*varlist)
      )
    )
    @model.add_instructions(Instruction.new("solve minimize obj"))
    return @model
  end
end
