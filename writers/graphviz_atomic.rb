require_relative '../cryptodag.rb'
require_relative '../operators/elementary.rb'


def get_shorthand_op_classname(c)
    case c.class.name
    when "XorOperator"
        return "+"
    when "SOperator"
        return "S"
    when "EqualityOperator"
        return "="
    when "MixColumnsOperator"
        return "MC"
    else
        # p c.class
        return "?"
    end
end

def get_graphviz(atoms, operators)
    res = ""
    res += "digraph {\n"
    # digraph options
    res +=  "\tgraph [rankdir=LR];\n"
    res +=  "\tnode [fontname=\"helvetica\"];\n"
    res +=  "\n"
    # display operators
    op_to_id = {}
    id_to_op = {}
    id = 0
    operators.each do |op|
        op_to_id[op] = id
        id_to_op[id] = op
        res += "\top_#{id} [shape=circle,label=\"#{get_shorthand_op_classname(op)}\"];\n"
        id += 1
    end
    # display atoms
    atom_to_id = {}
    id_to_atom = {}
    id = 0
    atoms.each do |atom|
        if !atom_to_id.key?(atom)
            atom_to_id[atom] = id
            id_to_atom[id] = atom
            res += "\tatom_#{id} [shape=box,label=\"#{atom.name}\"];\n"
            id += 1
        end
    end
    # display links between operators and atoms
    operators.each do |op|
        op.inputs.each do |atom|
            res += "\tatom_#{atom_to_id[atom]} -> op_#{op_to_id[op]};\n"
        end
        op.outputs.each do |atom|
            res += "\top_#{op_to_id[op]} -> atom_#{atom_to_id[atom]};\n"
        end
    end
    res +=  "}\n"
end


def writefile_graphviz(atoms, operators, filename)
    file = File.open(filename, "w")
    file.puts(get_graphviz(atoms, operators))
    file.close
end