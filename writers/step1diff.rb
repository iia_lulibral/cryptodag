require 'set'

require_relative "abstractwriter"
require_relative "../constraints/all"


class DiffHalfInfo
  attr_accessor :t, :i, :j, :k, :v

  def initialize(t:,i:,j:,k:,v:)
    @t=t
    @i=i
    @j=j
    @k=k
    @v=v
  end

  def v_id()
    return "#{@t}_#{@i}_#{@k}"
  end
end


## Diff model implementation
class Step1Diff < AbstractWriter

    def generate_extended_model(dag, rounds)
        generate_variables(dag)
        generate_constraints(dag)
        add_diff_variables(dag, rounds)
        generate_objective(dag)
    end

  def add_node_variables(node)
    name = node.name
    case node
    when ConstantNode
      matrix = ConstantTable.new(name, "int", [node.y, node.x], Array.new(node.x * node.y, 0))
      @model.add_variables(matrix)
      @variable_dict[node] = matrix
    else
      table = Table.new(name, "bool", [node.y, node.x])
      @model.add_variables(table)
      @variable_dict[node] = table
    end
  end

  ## adds diff variables (for each couple of mixcolumn states columns, diff in the column position)
  def add_diff_variables(dag, rounds)
    # compute diff variables
    var_diff_by_name = {}
    (rounds-1).times do |i1|
      (rounds-1).times do |i2|
        4.times do |j|
          4.times do |k|
            yname = "diff_y_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"
            vy = Variable.new(yname, "bool")
            var_diff_by_name[yname] = vy
            @model.add_variables(vy)

            zname = "diff_z_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"
            vz = Variable.new(zname, "bool")
            var_diff_by_name[zname] = vz
            @model.add_variables(vz)

            # kname = "diff_k_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"
            # vk = Variable.new(kname, "bool")
            # var_diff_by_name[kname] = vk
            # @model.add_variables(vk)
          end
        end
      end
    end
    # diff symmetry: diff_b1_b2 = diff_b2_b1 (C'7)
    (rounds-1).times do |i1|
      i1.times do |i2|
        4.times do |j|
          4.times do |k|
            @model.add_constraints(Equality.new(
              var_diff_by_name["diff_y_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_y_ia_#{i2}_j_#{j}_k_#{k}_ib_#{i1}"]
            ))
            @model.add_constraints(Equality.new(
              var_diff_by_name["diff_z_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_z_ia_#{i2}_j_#{j}_k_#{k}_ib_#{i1}"]
            ))
          end
        end
      end
    end
    # diff transitivity: diff_b1_b2 + diff_b2_b3 + diff_b1_b3 != 1 (C'8) 
     
    (rounds-1).times do |i1|
      (rounds-1).times do |i2|
        (rounds-1).times do |i3|
          if i3 > i2 
            4.times do |j|
              4.times do |k|
                @model.add_constraints(Diff1.new(
                  var_diff_by_name["diff_y_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i3}"],
                  var_diff_by_name["diff_y_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"],
                  var_diff_by_name["diff_y_ia_#{i2}_j_#{j}_k_#{k}_ib_#{i3}"]
                ))
                @model.add_constraints(Diff1.new(
                  var_diff_by_name["diff_z_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i3}"],
                  var_diff_by_name["diff_z_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"],
                  var_diff_by_name["diff_z_ia_#{i2}_j_#{j}_k_#{k}_ib_#{i3}"]
                ))
              end
            end
          end
        end
      end
    end

    # linking between diff and abstract byte (C'9)
    yvars = (rounds-1).times.collect{|i| nil}
    zvars = (rounds-1).times.collect{|i| nil}
    for n in dag.find_all_nodes().filter{|n| n.class == MixColumnNode }
      i = n.name.split("_")[1].to_i
      in_var = n.inputs.map { |inputnode| @variable_dict[inputnode] }[0]
      out_var = @variable_dict[n]
      yvars[i] = in_var
      zvars[i] = out_var
    end
    (rounds-1).times do |i1|
      (rounds-1).times do |i2|
        4.times do |j|
          4.times do |k|
            @model.add_constraints(Diff1.new(
              yvars[i1][j][k],
              yvars[i2][j][k],
              var_diff_by_name["diff_y_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"]
            ))
            @model.add_constraints(Diff1.new(
              zvars[i1][j][k],
              zvars[i2][j][k],
              var_diff_by_name["diff_z_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"]
            ))
          end
        end
      end
    end

    # create xvars variables
    xvars = (rounds).times.collect{|i| nil}
    xvars[0] = @variable_dict[dag.find_all_nodes().filter{|n| n.name == "addKey" }[0]]
    for n in dag.find_all_nodes().filter{|n| n.name.split("_")[0] == "endRound" }
      i = n.name.split("_")[1].to_i
      xvars[i+1] = @variable_dict[n]
    end
    # link diffz and xi and x_{i+1}
    # XOR(DIFFZ[i1, j, k, i2], X[i1 + 1, j, k], X[i2 + 1, j, k])
    (rounds-1).times do |i1|
      (rounds-1).times do |i2|
        4.times do |j|
          4.times do |k|
            @model.add_constraints(Diff1.new(
              var_diff_by_name["diff_z_ia_#{i1}_j_#{j}_k_#{k}_ib_#{i2}"],
              xvars[i1+1][j][k],
              xvars[i2+1][j][k]
            ))
          end
        end
      end
    end


    # Quasi-MDS property over differents rounds
    (rounds-1).times do |i1|
      i1.times do |i2|
        4.times do |k|
          model.add_constraints(IncludedIn.new(
            Sum.new(
              var_diff_by_name["diff_y_ia_#{i1}_j_#{0}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_y_ia_#{i1}_j_#{1}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_y_ia_#{i1}_j_#{2}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_y_ia_#{i1}_j_#{3}_k_#{k}_ib_#{i2}"],

              var_diff_by_name["diff_z_ia_#{i1}_j_#{0}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_z_ia_#{i1}_j_#{1}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_z_ia_#{i1}_j_#{2}_k_#{k}_ib_#{i2}"],
              var_diff_by_name["diff_z_ia_#{i1}_j_#{3}_k_#{k}_ib_#{i2}"],
            ),
            Set[0,4,5,6,7,8]
          ))
        end
      end
    end
    @var_diff_by_name = var_diff_by_name

    @model
  end

  def add_node_constraints(node)
    name = node.name
    modelinputvars = node.inputs.map { |inputnode| @variable_dict[inputnode] }
    outputvar = @variable_dict[node]
    case node
    when ConstantNode
      # nothing to do here
    when InputNode
      # nothing to do here
    when OutputNode
      # input[*] = output[*]
      outputvar.each_index do |*index|
        @model.add_constraints(
          Equality.new(modelinputvars[0][*index], outputvar[*index])
        )
      end
    when KeySeparationNode
      # input[*] = output[*]
      outputvar.each_index do |*index|
        @model.add_constraints(
          Equality.new(modelinputvars[0][*index], outputvar[*index])
        )
      end
    when XorNode
      outputvar.each_index do |*index|
        @model.add_constraints(Diff1.new(
          modelinputvars[0][*index],
          modelinputvars[1][*index],
          outputvar[*index]
        ))
      end
    when SubCellNode
      # input[*] = output[*]
      outputvar.each_index do |*index|
        @model.add_constraints(
          Equality.new(modelinputvars[0][*index], outputvar[*index])
        )
      end
    when ShuffleCellNode
      # apply permutation input[indices] = output[shuffle(indices)]
      outputvar.each_index do |*index|
        y,x = *index
        @model.add_constraints(Equality.new(
          outputvar[x,y],
          modelinputvars[0][*node.permuted_indices(*index)],
        ))
      end
    when MixColumnNode
      coeff_tab = [
        [[1, 2, 3], 0],
        [[0, 2, 3], 1],
        [[0, 1, 3], 2],
        [[0, 1, 2], 3],
      ]
      outputvar.dimensions[1].times do |dim2|
        coeff_tab.each do |coeff_line|
          model.add_constraints(Diff1.new(
            *(coeff_line[0].map { |coeff_input| modelinputvars[0][coeff_input, dim2] } + [outputvar[coeff_line[1], dim2]])
          ))
        end
      end
      v = modelinputvars[0]
      w = outputvar
      4.times do |j| 
        model.add_constraints(IncludedIn.new(
          Sum.new(
            v[0,j],v[1,j],v[2,j],v[3,j], w[0,j],w[1,j],w[2,j],w[3,j]
            # v[j,0],v[j,1],v[j,2],v[j,3], w[j,0],w[j,1],w[j,2],w[j,3]
          ),
          Set[0,4,5,6,7,8]
        ))
      end
      4.times do |j|
        model.add_constraints(Equivalence.new(
          Equals.new(Sum.new(v[0,j],v[1,j],v[2,j],v[3,j]), 0),
          Equals.new(Sum.new(w[0,j],w[1,j],w[2,j],w[3,j]), 0)
          # Equals.new(Sum.new(v[j,0],v[j,1],v[j,2],v[j,3]), 0),
          # Equals.new(Sum.new(w[j,0],w[j,1],w[j,2],w[j,3]), 0)
        ))
      end
      # res += "constraint forall(j in 0..3) (\n";
      # res += "\tsum(i in 0..3)(#{b}[i,j])==0 <-> sum(i in 0..3)(#{a}[i,j])==0\n"
      # res += ");\n"
    else
      raise "ConstraintWriter.process_node: unknown node: #{node.class}"
    end
    return @model
  end

  def generate_objective(dag)
    varlist = dag.find_all_nodes().filter { |node|
      node.class == SubCellNode
    }.map { |node|
      @variable_dict[node].to_a()
    }.flatten()
    obj = Variable.new("obj", 1..20)
    @model.add_variables(obj)
    @model.add_constraints(
      Equality.new(
        obj,
        Sum.new(*varlist)
      )
    )
    @model.add_instructions(Instruction.new("solve satisfy"))
    # solve :: seq_search(
    #   [int_search([obj],input_order, indomain_min, complete),
      
    #   int_search(
    #             [obj] ++
    #             [ DX[r,i,j] | r in 0..n-1, i in 0..3, j in 0..3] ++
    #             [ DY[r,i,j] | r in 0..n-2, i in 0..3, j in 0..3]  ++
    #             [ DZ[r,i,j] | r in 0..n-2, i in 0..3, j in 0..3] ++
    #             [ DK[i,j] | i in 0..3, j in 0..3] 
      
      
    #    ,dom_w_deg, indomain_min, complete)
    #   ])

    # puts @var_diff_by_name.keys
    # diff_list = @var_diff_by_name.keys.map{|e| e}.compact.join(", ")
    # original_var_list = []
    # for n in dag.find_all_nodes()
    #   if n.class != ConstantNode
    #     vars = @variable_dict[n].to_a()
    #     for v in vars
    #       original_var_list.append("#{v.table.name}[#{v.indexes.map{|i|i+1}.join(",")}]")
    #     end
    #   end
    # end
    # original_var_list_text = original_var_list.join(", ")
    # @model.add_instructions(Instruction.new("
    #   constraint obj=#{4};
    #   solve::seq_search([
    #     int_search([obj], input_order, indomain_min, complete),
    #     int_search(
    #       [obj]++
    #       [#{original_var_list_text}]
    #       , dom_w_deg, indomain_min, complete
    #     )
    #   ])
    #   satisfy"))
    #   # minimize obj"))
    return @model
  end
end
