require 'rgl/adjacency'
require 'rgl/dot'
require 'rgl/topsort'

require_relative "./cryptodag.rb"

##
# Computes the values obtained by computing 
# @param input_values [Vec<u8>] set of values for each variable
# @param input_variables [Vec<Variable>] set of input variables values
# @param output_variables [Vec<Variable>] set of output variables
# @param operators [Vec<Operators>] set of operators
def compute_set_of_operators(input_values, input_variables, output_variables, operators)
    raise "compute_set_of_operators: inconsistent input_variables (#{input_values.length} should be of size #{input_variables.length})" unless input_values.length == input_variables.length
    if operators.length == 0
        return input_values
    end
    # executes a topological sort to find the order to execute operators
    adj_list = []
    operators.each do |e|
        e.inputs.each do |a|
            adj_list.push(a) # input comes before the operator
            adj_list.push(e)
        end
        e.outputs.each do |a|
            adj_list.push(e)
            adj_list.push(a) # output comes after the operator
        end
    end
    
    # feed a variable map with initial values
    value_map = {} # associates variables to their value
    input_values.length.times do |i|
        value_map[input_variables[i]] = input_values[i]
    end
    # register variables where a value is already registered
    for e in adj_list
        if e.class <= Variable
            if e.value != nil
                value_map[e] = e.value
            end
        end
    end
    for k in value_map.keys
        if value_map[k] == nil
            raise "simulate_cryptodag: ERROR value_map contains a nil value"
        end
    end
    dg = RGL::DirectedAdjacencyGraph[*adj_list]
    sorted_vertices = dg.topsort_iterator.to_a
    sorted_vertices = sorted_vertices.filter{|v| v.class <= CryptoOperator}

    # execute operators
    sorted_vertices.each do |op|
        raise "compute_set_of_operators: should be a CryptoOperator (current #{op.class})" unless op.class <= CryptoOperator
        values = []
        op.inputs.each do |e|
            if value_map.include?(e)
                values.push(value_map[e])
            else
                raise "compute_set_of_operators: bad topological sort (variable #{e} should be computed)"
            end
        end
        # p "op:#{op.class} input values: #{values}"
        outputs = op.compute(values)
        # p "op:#{op.class} output values: #{outputs}"
        outputs.length.times do |i|
            if !value_map.include?(op.outputs[i])
                value_map[op.outputs[i]] = outputs[i]
            end
        end
    end
    # return operators values
    return output_variables.map {|v| value_map[v]}
end

