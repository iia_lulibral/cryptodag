require_relative "../cryptodag.rb"
require_relative "../constraints/constraints"
require_relative "../constraints/variables"
require_relative "../constraints/expression"

## equality operator
class EqualityOperator < CryptoOperator

  ## Build an Equality  ( "a = b" )
  # @param input [ Expression ]
  # @param output [ Variable ]
  def initialize(input, output)
    super(
      inputs: [input],
      outputs: [output],
      constraints: [Equality.new(input, output)],
      global_constraints: [],
    )
  end

  def compute(values)
    check_inputs(values)
    outputs = [values[0]]
    check_outputs(values)
    return outputs
  end
end

## xor operator
class XorOperator < CryptoOperator
  ## builds a Xor operator (a xor b = c)
  # @param inputs [Vec<Expression>]
  # @param output [Variable]
  def initialize(*inputs, output)
    super(
      inputs: inputs,
      outputs: [output],
      constraints: [Equality.new(
        Xor.new(*inputs),
        output
      )],
      global_constraints: [],
    )
  end

  def compute(values)
    check_inputs(values)
    outputs = [values.reduce(0, :^)]
    check_outputs(outputs)
    return outputs
  end
end

## S operator
class SOperator < CryptoOperator
  ## builds a S operator
  # @param input [ Expression ]
  # @param output [ variable ]
  def initialize(input, output, s_array)
    @s_array = s_array
    super(
      inputs: [input],
      outputs: [output],
      constraints: [TableConstraint.new(
        [input, output],
        s_array.length.times.map { |i| [i, s_array[i]] }
      )],
      global_constraints: ["table"],
    )
  end

  def compute(values)
    check_inputs(values)
    outputs = [@s_array[values[0]]]
    check_outputs(outputs)
    return outputs
  end
end

## MixColumns operator
class MixColumnsOperator < CryptoOperator
  ##
  # @params inputs [Vec<variable>] 4 byte input
  # @params outputs [Vec<variable>] 4 byte output
  # @params m [Vec<u8>] mixcolumns matrix vector
  def initialize(inputs, outputs, m)
    raise "MixColumnsOperator: matrix dimensions should be of the same size of the input (current y size: #{m.length})" unless inputs.length == m.length
    raise "MixColumnsOperator: matrix dimensions should be of the same size of the input (current x size: #{m..first.length})" unless inputs.length == m.first.length
    raise "MixColumnsOperator: output dimensions should be of the same size of the input (current size: #{outputs.length})" unless inputs.length == outputs.length
    @m = m
    constraints = []
    inputs.length.times do |i| # add a constraint per output
      constraints.push(Equality.new(
        Xor.new(*inputs.length.times.map{ |j|
          galois_field_multiply(inputs[j], m[i][j])
        }),
        outputs[i]
      ))
    end
    super(
      inputs: inputs,
      outputs: outputs,
      constraints: constraints,
      global_constraints: [],
    )
  end

  ##
  # @param e [Expression] value to multiply by the matrix element
  # @param m [u8] value used to multiply
  # @return [Expression] that multiplies e by m in the galois field
  def galois_field_multiply(e, m)
    if m == 0
      return 0
    elsif m == 1
      return e
    elsif m == 2
      return Modulo.new(Mult.new(e, 2), 256)
    elsif m == 3
      return Modulo.new(
               Xor.new(Mult.new(e, 2), e),
               256
             )
    else
      raise "galois_field_multiply: m=#{m} not supported yet (for now, it should be <= 3)"
    end
  end

  def compute(values)
    check_inputs(values)
    outputs = []
    @inputs.length.times do |i|
      tmp = 0
      @inputs.length.times do |j|
        tmp = tmp ^ g_mul(values[j], @m[i][j])
      end
      outputs.push(tmp)
    end
    check_outputs(outputs)
    return outputs
  end
end


## Galois Field multiplication (a.b)
# @return a.b
def g_mul(a, b)
  raise "g_mul: a is nil" unless a != nil
  raise "g_mul: b is nil" unless b != nil
  p = 0
  8.times do |counter|
    if (b&1) != 0
      p ^= a;
    end
    hi_bit_set = (a&0x80) != 0
    a <<= 1
    if hi_bit_set
      a ^= 0x1B
    end
    b >>= 1
  end
  return p % 256
end