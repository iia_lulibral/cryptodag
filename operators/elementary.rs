

fn op_equality_compute(inputs:Vec<u8>) -> Vec<u8> {
    return inputs[0];
}

fn op_xor_compute(inputs:Vec<u8>) -> Vec<u8> {
    return vec![inputs.reduce((^), 0)];
}

// ...


fn computeAbstractionTable(
    operator:Fn(Vec<u8>)->Vec<u8>,
    dim_input: usize,
    dim_output: usize
) -> Vec<Vec<bool>> {
    cartesian_product!(...) {
        operator([params_input])
    }
}


// examples

computeAbstractionTable(op_equality_compute, 1, 1); // [[0,0], [1,1]]
computeAbstractionTable(op_xor_compute, 2, 1);  // xor3 -> [[0,0,0],[1,1,0],...]
computeAbstractionTable(op_xor_compute, 3, 1);  // xor4 -> [...]