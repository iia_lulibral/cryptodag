# ShiftRows operator.

require_relative "../cryptodag.rb"
require_relative "../operators/elementary.rb"


class ShiftRowsNode < CryptoDagNode
  def initialize(name:, input:)
    output = DTable.new("#{name}", input.type, input.dimensions)
    operators = []
    y_size = output.dimensions[0]
    x_size = output.dimensions[1]
    y_size.times do |y|
        x_size.times do |x|
            operators.push(EqualityOperator.new(input[y][(y+x) % x_size], output[y][x]))
        end
    end
    super(inputs: [input], outputs: [output], operators: operators, name: name)
  end
end
