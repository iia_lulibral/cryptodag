# IndexedSubBytes operator. Takes as a parameter a function that applies a substitution matrix depending on indices

require_relative "../cryptodag.rb"
require_relative "../operators/elementary.rb"

class IndexedSubBytesNode < CryptoDagNode

  ##
  # @param name [String] name of the node
  # @param input [DTable<Variable>] input variables
  # @param subtable_function [(indices:Vec<usize>)->Vec<u8>] table that maps bytes to be substitued
  def initialize(name:, input:, subtable_function:)
    output = DTable.new("#{name}", input.type, input.dimensions)
    operators = []
    @subtable_function = subtable_function
    input.each_index do |*index|
    #   raise "#{@subtable_function}" 
      s_table = @subtable_function.call(index)
      operators.push(SOperator.new(
        input[*index],
        output[*index],
        s_table
      ))
    end
    super(inputs: [input], outputs: [output], operators: operators, name: name)    
  end
end
