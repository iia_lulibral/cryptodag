# ShuffleCell operator. 

require_relative '../cryptodag.rb'
require_relative "../operators/elementary.rb"


class ShuffleCellNode < CryptoDagNode

    def initialize(name:, input:, permutation:)
        output = DTable.new("#{name}", input.type, input.dimensions)
        operators = []
        @permutation = permutation
        @y_size = input.dimensions[0]
        @x_size = input.dimensions[1] 
        raise "ShuffleCellNode: incorrect permutation size" unless @y_size*@x_size == permutation.length
        @y_size.times do |y|
            @x_size.times do |x|
                row, col = permuted_indices(y, x)
                operators.push(EqualityOperator.new(input[row][col], output[y][x]))
            end
        end
        super(inputs: [input], outputs: [output], operators: operators, name: name)
    end

    def permuted_indices(i,j)
        perm = @permutation[j*@y_size+i]
        return [perm % @y_size, perm / @y_size]
    end

end

