require_relative "../cryptodag.rb"
require_relative "../constraints/tables.rb"

## defines an input node (usually the plain text message or the key)
# simply returns the value it inputs
class InputNode < CryptoDagNode

  ## Initialize
  # @param name [ String ] Name of the Table
  # @param type [ Type ]   : Type of the variables
  # @param dimensions [ Array(int) ]  : dimensions of the Matrix
  # @param values [ Array ] : values of the variable
  def initialize(name:, type: 0..255, dimensions:, values: nil)
    # create output variables
    output = DTable.new(name, type, dimensions, values, constant:(values!=nil))
    # output.declarationMode = :unfolded
    # if name == "Cst_1"
    #   p output
    # end
    if values != nil
      dimensions[0].times do |i|
        dimensions[1].times do |j|
          output[i][j].value = values[i][j]
        end
      end
    end

    super(inputs: [], outputs: [output], operators: [], name: name)
  end
end
