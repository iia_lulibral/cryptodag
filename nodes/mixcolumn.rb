# MixColumn operator.

require_relative "../cryptodag.rb"
require_relative "../operators/elementary.rb"

class MixColumnNode < CryptoDagNode
  def initialize(name:, input:, m:)
    output = DTable.new("#{name}", input.type, input.dimensions)
    @m = m
    operators = []
    input.dimensions[1].times do |x|
      operators.push(MixColumnsOperator.new(
        input.dimensions[0].times.map{|y| input[y][x]},
        output.dimensions[0].times.map{|y| output[y][x]},
        m
      ))
    end
    super(inputs: [input], outputs: [output], operators: operators, name: name)
  end
end
