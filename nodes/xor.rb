require_relative "../cryptodag.rb"
require_relative "../util.rb"
require_relative "../constraints/tables.rb"
require_relative "../operators/elementary.rb"

# def initialize(name:, type: 0..255, dimensions:, values: nil)
#     # cre=ate out.put variables.,
#     output = DTable.new(name, type, dimensions, values)
#     super([], [output], [], name)
#   end

## xor node.
# performs a bitwise xor on all the given input
class XorNode < CryptoDagNode

  ##
  # @param name [String]
  # @param inputs [Vec<DTable>]
  def initialize(name:, inputs:)
    raise "XorNode: At least two inputs are needed" unless inputs.length() >= 2
    inputs.map { |vars| vars.dimensions }.uniq.size == 1
    # x = inputs[0].x
    # y = inputs[0].y
    # inputs.each() do |e|
    #     raise "XorNode: Different input sizes" unless e.x == x && e.y == y
    # end
    # create output variables
    output = DTable.new("#{name}_out", inputs.first.type, inputs.first.dimensions)
    # define operators here
    operators = []
    inputs.first.each_index do |*index|
      op = XorOperator.new(
        *(inputs.map { |e| e[*index] }),         output[*index]
      )
      operators.push(op)
    end
    super(inputs: inputs, outputs: [output], operators: operators, name: name)
  end
end
