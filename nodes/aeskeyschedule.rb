# AES Key Schedule operator. Takes as a parameter a key and expands it

require_relative "../cryptodag.rb"
require_relative "../operators/elementary.rb"

class AESKeyScheduleNode < CryptoDagNode

  ##
  # @param name [String]: name of the node
  # @param input [DTable<Variable> of size 4x4] input variables
  # @param subtable [Vec<u8> of size |u8|] byte substitution table
  # @param rcon [u8]: round constant
  def initialize(name:, input:, subtable:, rcon:)
    raise "AESKeyScheduleNode: incorrect input size (#{input.dimensions} should be [4,X])" unless input.dimensions[0] == 4
    nb_cols = input.dimensions[1] # computes nb_cols as it depends on the AES version (128,192,256)
    output = DTable.new("#{name}", input.type, input.dimensions)
    operators = []
    @subtable = subtable
    # apply rotation (first byte becomes last, second first, ...)
    @rotated_bytes = [
        Variable.new("#{name}_rot_0", 0..255),
        Variable.new("#{name}_rot_1", 0..255),
        Variable.new("#{name}_rot_2", 0..255),
        Variable.new("#{name}_rot_3", 0..255),
    ]
    4.times do |i|
        operators.push(EqualityOperator.new(
            input[(i+1) % 4][nb_cols-1], # last column of the input
            @rotated_bytes[i],
        ))
    end
    # apply substitution
    @sub_rotated_bytes = [
        Variable.new("#{name}_sub_rot_0", 0..255),
        Variable.new("#{name}_sub_rot_1", 0..255),
        Variable.new("#{name}_sub_rot_2", 0..255),
        Variable.new("#{name}_sub_rot_3", 0..255),
    ]
    4.times do |i|
        operators.push(SOperator.new(
            @rotated_bytes[i],
            @sub_rotated_bytes[i],
            @subtable
        ))
    end
    @rcon_col = [
        Variable.new("rcon_#{name}_0", 0..256, value=rcon),
        Variable.new("rcon_#{name}_0", 0..256, value=0),
        Variable.new("rcon_#{name}_0", 0..256, value=0),
        Variable.new("rcon_#{name}_0", 0..256, value=0)
    ]
    # construct first column of the output (sub_rotated_bytes XOR [first input col] XOR [Rcon,0,0,0])
    4.times do |i| 
        operators.push(XorOperator.new(
            @sub_rotated_bytes[i],
            input[i][0],
            @rcon_col[i],
            output[i][0]
        ))
    end
    @sub_vars = []
    (nb_cols-1).times do |i|
        index = i+1 # index in [1..3]
        if nb_cols > 6 && index % nb_cols == 4 # if AES256 apply additional S-box
            @sub_vars = [
                Variable.new("#{name}_sub_intermediate_0", 0..255),
                Variable.new("#{name}_sub_intermediate_1", 0..255),
                Variable.new("#{name}_sub_intermediate_2", 0..255),
                Variable.new("#{name}_sub_intermediate_3", 0..255),
            ]
            4.times do |j|
                operators.push(SOperator.new(
                    output[j][index-1],
                    @sub_vars[j],
                    @subtable
                ))
                operators.push(XorOperator.new(
                    input[j][index],
                    @sub_vars[j],
                    output[j][index]
                ))
            end
        else
            4.times do |j| # for each row of the column
                operators.push(XorOperator.new(
                    input[j][index],
                    output[j][index-1],
                    output[j][index]
                ))
            end
        end
    end
    super(inputs: [input], outputs: [output], operators: operators, name: name)    
  end

  def all_atoms()
    return flatten_output(0)+@sub_vars+@rcon_col+@sub_rotated_bytes+@rotated_bytes
  end
end
