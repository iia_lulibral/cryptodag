# SubBytes node. Takes as a parameter a substitution matrix and applies it to the original matrix

require_relative "../cryptodag.rb"
require_relative "../operators/elementary.rb"

class SubBytesNode < CryptoDagNode

  ##
  # @param name [String] name of the node
  # @param input [DTable<Variable>] input variables
  # @param subtable [Vec<u8>] table that maps bytes to be substitued
  def initialize(name:, input:, subtable:)
    output = DTable.new("#{name}", input.type, input.dimensions)
    operators = []
    @subtable = subtable
    # raise "SubBytesNode: incorrect subtable size" unless ...
    input.each_index do |*index|
      operators.push(SOperator.new(
        input[*index],
        output[*index],
        @subtable
      ))
    end
    super(inputs: [input], outputs: [output], operators: operators, name: name)    
  end
end
