# represents a cryptosystem using a DAG.
# Each node corresponds to an operation. Nodes have possibly one or many output states (n x m byte matrix)
#
#
# HOW TO TAME YOUR ABSTRACTION (GENERIC WAY!) [INPUTS: cryptodag, abstraction] -> abstract minizinc model (step1)
# 1. for each CryptoOperator, create an AbstractCryptoOperator (create AbstractCryptoDag)
# 2. generate AbstractionTables for each CryptoOperator (value enumeration over operator inputs)
# 3. [OPTIONAL] infer additional refinements
# 4. write model in minizinc

require "set"

require_relative "constraints/constraints"
require_relative "constraints/variables"
require_relative "constraints/model"


## represents an elementary operator (Equality, S-box, )
# # examples
# (=) a -> b
# (xor) a b -> c
# (S_{sub}) a -> b  ( sub[0] = S(0), sub[1] = S(1) )
# (MC_{M}) a b c d -> e f g h
class CryptoOperator
  attr_accessor :inputs # vector of input variables
  attr_accessor :outputs # vector of output variables
  attr_accessor :constraints # constraints implementing the operator
  attr_accessor :global_constraints # global constraints that should be registered while creating the CP model

  ##
  # @param inputs [Vec<Variable>]
  # @param outputs [Vec<Variable>]
  # @param constraints [Vec<Constraint>]
  # @param global_constraints [Vec<String>]
  protected def initialize(inputs:, outputs:, constraints:, global_constraints:)
    inputs.each do |e|
      raise "CryptoOperator: inputs should be of type <= Variable (current class: #{e.class})" unless e.class <= Variable
    end
    outputs.each do |e|
      raise "CryptoOperator: outputs should be of type <= Variable (current class: #{e.class})" unless e.class <= Variable
    end
    constraints.each do |e|
      raise "CryptoOperator: constraint should be of type Constraint" unless e.class < Constraint
    end
    global_constraints.each do |e|
      raise "CryptoOperator: global_constraints should be of type String" unless e.class == String
    end
    
    @inputs = inputs
    @outputs = outputs
    @constraints = constraints
    @global_constraints = global_constraints
  end

  def register_to_model(model)
    raise "CryptoOperator: model should be of class Model" unless model.kind_of?(Model)
    model.add_constraints(*@constraints)
  end


  protected def check_inputs(values)
    raise "CryptoOperator.check_inputs: values do not match (#{@values.length} should be #{@inputs.length})" unless values.length == @inputs.length
    for e in values
      if e == nil
        raise "CryptoOperator.check_inputs: values contain some nil: #{values}"
      end
    end
    # TODO(all) check domains
  end

  protected def check_outputs(values)
    raise "CryptoOperator.check_outputs: values do not match (#{@values.length} should be #{@outputs.length})" unless values.length == @outputs.length
    for e in values
      if e == nil
        raise "CryptoOperator.check_outputs: values contain some nil: #{values}"
      end
    end
    # TODO(all) check domains
  end

  ##
  # @param values that should be the same size of the inputs (and same domains)
  # @return output values
  #
  def compute(values)
    raise "CryptoOperator: compute not implemented in child class"
  end
end

## represents a DAG
class CryptoDagNode
  attr_accessor :name, :inputs, :outputs, :operators

  ##
  # @param inputs [Vec<Dtable>]
  # @param outputs [Vec<Dtable>]
  # @param operators [Vec<Operator>] linked to the given input and output Variables
  # @param name [String] name of the operator
  protected def initialize(inputs:, outputs:, operators:, name:)
    @name = name
    @inputs = inputs
    @outputs = outputs
    @operators = operators
  end

  ##
  # @param i [usize] input number (should be < than the input size)
  # @return [Vec<Variable>] flattened input
  def flatten_input(i)
    res = []
    @inputs[i].each_index do |*index|
      res.push(@inputs[i][*index])
    end
    return res
  end

  ##
  # @param i [usize] output number (should be < than the output size)
  # @return [Vec<Variable>] flattened output
  def flatten_output(i)
    res = []
    @outputs[i].each_index do |*index|
      res.push(@outputs[i][*index])
    end
    return res
  end
end

# represents a CryptoDag. Useful to get the graphviz visualization, etc.
# @param input_nodes [Vec<CryptoDagNode>]
# @param output_nodes [Vec<CryptoDagNode>]
# @param nodes [Vec<CryptoDagNode>] includes all nodes of the dag
#
class CryptoDag
  def initialize(input_nodes, output_nodes, nodes)
    @input_nodes = input_nodes
    @output_nodes = output_nodes
    @nodes = nodes
  end

  ## returns operators used by the cryptodag
  # @returns [Vec<CryptoOperator>]
  def operators()
    return @nodes.map{|n| n.operators}.reduce([], :+)
  end

  ## returns a list of atoms used by the cryptodag
  # @returns [Vec<CryptoAtom=Variable>]
  def atoms()
    return operators().map{|op| op.inputs.flatten + op.outputs.flatten}.flatten.uniq
  end

end